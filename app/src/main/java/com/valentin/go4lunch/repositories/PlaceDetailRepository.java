package com.valentin.go4lunch.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.apis.RestaurantAPIService;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.apis.UsersLikesApi;
import com.valentin.go4lunch.models.Place;
import com.valentin.go4lunch.models.placedetail.PlaceResultDetail;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.UserLike;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceDetailRepository implements PlaceDetailRepositoryContrat {

    private static final String LOG = PlaceDetailRepository.class.getSimpleName();
    private final RestaurantAPIService service = RetrofitClientInstance.getRetrofitInstance().create(RestaurantAPIService.class);

    private final MutableLiveData<Restaurant> restaurantMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<User>> usersInterestedLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isRestaurantLikedLiveData = new MutableLiveData<>();
    private final MutableLiveData<User> userLiveData = new MutableLiveData<>();

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return restaurantMutableLiveData;
    }

    @Override
    public void getRestaurantLiveData(String place_id, String fields, String api_key) {

        service.getRestaurant(place_id, fields, api_key).enqueue(new Callback<PlaceResultDetail>() {
            @Override
            public void onResponse(@NonNull Call<PlaceResultDetail> call, @NonNull Response<PlaceResultDetail> response) {
                if(response.body() != null) {
                    Place place = response.body().getPlace();
                    Restaurant restaurant = new Restaurant();
                    restaurant.setPlaceId(place.getPlaceId());
                    restaurant.setName(place.getName());
                    restaurant.setAddress(place.getVicinity());
                    restaurant.setOpenNow(place.getOpeningHours().getOpenNow());
                    restaurant.setRating(place.getRating());
                    restaurantMutableLiveData.setValue(restaurant);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceResultDetail> call, @NonNull Throwable t) {
                restaurantMutableLiveData.setValue(null);
                t.printStackTrace();
            }
        });

    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return usersInterestedLiveData;
    }

    @Override
    public void getUsersInterestedByRestaurant(String placeId) {

        ArrayList<User> users = new ArrayList<>();

        UserApi.getUsers()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        List<DocumentSnapshot> documents = task.getResult().getDocuments();
                        if(documents.size() != 0) {

                            for(DocumentSnapshot document : documents) {
                                User user = document.toObject(User.class);
                                if(user != null && user.getChoiceRestaurant() != null) {
                                    if(user.getChoiceRestaurant().equalsIgnoreCase(placeId)) {
                                        users.add(user);
                                    }
                                }
                            }

                            usersInterestedLiveData.setValue(users);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    usersInterestedLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    @Override
    public void createUserLike(UserLike userLike) {
        UsersLikesApi.createUserLike(userLike);
    }

    @Override
    public LiveData<Boolean> isRestaurantLiked(String uid, String restaurant_id) {
        return UsersLikesApi.isRestaurantLiked(uid, restaurant_id);
    }

    @Override
    public void removeLike(UserLike userLike) {
        UsersLikesApi.removeLike(userLike);
    }

    @Override
    public LiveData<User> subscribeUserLiveData() {
        return userLiveData;
    }

    @Override
    public void getUser(String uid) {
        Log.d(LOG, "[LOG] --> Récupération de l'utilisateur " + uid + " via FIRESTORE !");
        UserApi.getUser(uid)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if(document != null && document.exists()) {
                            User user = document.toObject(User.class);
                            userLiveData.setValue(user);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    userLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    @Override
    public void updateUser(User user) {
        UserApi.updateUser(user);
    }
}
