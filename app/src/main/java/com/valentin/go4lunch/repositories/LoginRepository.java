package com.valentin.go4lunch.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.models.User;

public class LoginRepository implements LoginRepositoryContrat {

    private static final String LOG = LoginRepository.class.getSimpleName();

    private final MutableLiveData<DocumentSnapshot> userLiveData = new MutableLiveData<>();

    @Override
    public LiveData<DocumentSnapshot> subscribeUserLiveData() {
        return userLiveData;
    }

    @Override
    public void getUser(String uid) {
        Log.d(LOG, "[LOG] --> Récupération de l'utilisateur " + uid + " via FIRESTORE !");
        UserApi.getUser(uid)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        userLiveData.setValue(document);
                    }
                })
                .addOnFailureListener(e -> {
                    userLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    @Override
    public void createUser(User user) {
        Log.d("UserService", "[LOG] --> Création de l'utilisateur via FIRESTORE !");
        UserApi.createUser(user);
    }
}
