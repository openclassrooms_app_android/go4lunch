package com.valentin.go4lunch.repositories;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.apis.RestaurantAPIService;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.models.Place;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.placeinfo.Photo;
import com.valentin.go4lunch.models.placeinfo.PlaceResult;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.wrappers.DataWrapperMap;
import com.valentin.go4lunch.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapRepository implements MapRepositoryContrat {

    private static final String LOG = MapRepository.class.getSimpleName();

    private static final String PLACE_LOCATION = "PLACE_LOCATION";
    private final RestaurantAPIService restaurantAPIService = RetrofitClientInstance.getRetrofitInstance().create(RestaurantAPIService.class);

    private final MediatorLiveData<ArrayList<DataWrapperMap>> mediatorLiveData = new MediatorLiveData<>();
    private final ArrayList<DataWrapperMap> dataWrapperMapArrayList = new ArrayList<>();
    private ArrayList<User> users;

    private final MutableLiveData<ArrayList<Restaurant>> restaurantsMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<User>> usersLiveData = new MutableLiveData<>();

    public MapRepository() {
        mediatorLiveData.addSource(usersLiveData, value -> {
            users = value;
        });

        mediatorLiveData.addSource(restaurantsMutableLiveData, restaurants -> {
            if(users != null && restaurants != null) {
                for(Restaurant restaurant : restaurants) {

                    boolean isBooked = false;
                    for (int i = 0; i < users.size() && !isBooked; i++) {

                        if(users.get(i).getChoiceRestaurant() != null && !TextUtils.isEmpty(users.get(i).getChoiceRestaurant())) {
                            if(users.get(i).getChoiceRestaurant().equalsIgnoreCase(restaurant.getPlaceId())) {
                                isBooked = true;
                            }
                        }

                        DataWrapperMap dataWrapperMap = new DataWrapperMap(restaurant, isBooked);
                        dataWrapperMapArrayList.add(dataWrapperMap);
                    }

                }
                mediatorLiveData.setValue(dataWrapperMapArrayList);
            }
        });
    }

    @Override
    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return restaurantsMutableLiveData;
    }

    @Override
    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {

        restaurantAPIService.getNearbyRestaurant(location, radius, type, api_key).enqueue(new Callback<PlaceResult>() {
            @Override
            public void onResponse(@NonNull Call<PlaceResult> call, @NonNull Response<PlaceResult> response) {
                if (response.body() != null) {

                    ArrayList<Restaurant> restaurants = new ArrayList<>();

                    ArrayList<Place> places = response.body().getPlaces();
                    if(places.size() != 0) {

                        for (int i = 0; i < places.size(); i++) {

                            Location placeLocation = new Location(PLACE_LOCATION);
                            placeLocation.setLatitude(places.get(i).getGeometry().getLocation().getLat());
                            placeLocation.setLongitude(places.get(i).getGeometry().getLocation().getLng());

                            Place place = places.get(i);
                            Restaurant restaurant = new Restaurant();
                            restaurant.setPlaceId(place.getPlaceId());
                            restaurant.setName(place.getName());
                            restaurant.setAddress(place.getVicinity());
                            restaurant.setLatLng(new LatLng(place.getGeometry().getLocation().getLat(), place.getGeometry().getLocation().getLng()));
                            restaurant.setOpenNow(place.getOpeningHours() != null ? place.getOpeningHours().getOpenNow() : false);
                            restaurant.setRating(place.getRating() != null ? place.getRating() : 0);
                            restaurant.setInternationalPhoneNumber(place.getInternationalPhoneNumber());
                            restaurant.setWebsite(place.getWebsite());
                            restaurant.setDistance(calculationByDistance(context, placeLocation));

                            Photo photo = null;
                            photo = places.get(i).getPhotos() != null ? places.get(i).getPhotos().get(0) : null;
                            if (context != null && photo != null) {
                                String photoUrl = context.getResources().getString(R.string.url_api_google_place) + "photo?"
                                        + RetrofitClientInstance.PHOTO_REFERENCE_PARAMETER + "="
                                        + photo.getPhotoReference() + "&" + RetrofitClientInstance.MAX_WIDTH_PARAMETER + "=400&"
                                        + RetrofitClientInstance.MAX_HEIGHT_PARAMETER + "=400&"
                                        + RetrofitClientInstance.KEY_PARAMETER + "=" + context.getResources().getString(R.string.google_api_key);

//                                    photoUrl = "https://www.caffemazzo.fr/wp-content/uploads/2020/11/logo-mazzo.png";

                                restaurant.setPhotoUrl(photoUrl);
                            }

                            restaurants.add(restaurant);
                        }

                    }

                    restaurantsMutableLiveData.setValue(restaurants);

                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceResult> call, @NonNull Throwable t) {
                restaurantsMutableLiveData.setValue(null);
                t.printStackTrace();
            }
        });

    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return usersLiveData;
    }

    @Override
    public void getUsers() {
        ArrayList<User> users = new ArrayList<>();

        UserApi.getUsers()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        List<DocumentSnapshot> documents = task.getResult().getDocuments();
                        if(documents.size() != 0) {
                            for(DocumentSnapshot document : documents) {
                                users.add(document.toObject(User.class));
                            }

                            usersLiveData.setValue(users);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    usersLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    public LiveData<ArrayList<DataWrapperMap>> subscribeDataMap() {
        return mediatorLiveData;
    }

    public double calculationByDistance(Context activityContext, Location placeLocation) {
        double distance = 0;
        if(activityContext != null) {
            distance = MainActivity.getLocation(activityContext.getApplicationContext()).distanceTo(placeLocation);
        }
        return distance;
    }
}
