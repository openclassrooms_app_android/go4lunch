package com.valentin.go4lunch.repositories;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;

import java.util.ArrayList;

public interface WorkmatesRepositoryContrat {
    LiveData<Restaurant> subscribeRestaurantLiveData();
    void getRestaurantLiveData(String place_id, String api_key);

    LiveData<ArrayList<User>> subscribeUsersLiveData();
    void getUsers();
}
