package com.valentin.go4lunch.repositories;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.UserLike;

import java.util.ArrayList;

public interface PlaceDetailRepositoryContrat {
    LiveData<Restaurant> subscribeRestaurantLiveData();
    void getRestaurantLiveData(String place_id, String fields, String api_key);

    LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData();
    void getUsersInterestedByRestaurant(String placeId);

    void createUserLike(UserLike userLike);
    LiveData<Boolean> isRestaurantLiked(String uid, String restaurant_id);
    void removeLike(UserLike userLike);

    LiveData<User> subscribeUserLiveData();
    void getUser(String uid);
    void updateUser(User user);
}
