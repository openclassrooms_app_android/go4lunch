package com.valentin.go4lunch.repositories;

import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.models.User;

public interface LoginRepositoryContrat {
    LiveData<DocumentSnapshot> subscribeUserLiveData();
    void getUser(String uid);
    void createUser(User user);
}
