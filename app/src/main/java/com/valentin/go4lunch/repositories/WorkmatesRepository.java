package com.valentin.go4lunch.repositories;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.BuildConfig;
import com.valentin.go4lunch.apis.RestaurantAPIService;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.models.placedetail.PlaceResultDetail;
import com.valentin.go4lunch.models.Place;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.wrappers.DataWrapperWorkmates;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkmatesRepository implements WorkmatesRepositoryContrat {

    private static final String LOG = WorkmatesRepository.class.getSimpleName();

    private final RestaurantAPIService restaurantAPIService = RetrofitClientInstance.getRetrofitInstance().create(RestaurantAPIService.class);

    private final MutableLiveData<Restaurant> restaurantMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ArrayList<User>> usersLiveData = new MutableLiveData<>();


    private int usersInterestedSize = 0;
    private int requestCompleted = 0;
    private final MediatorLiveData<ArrayList<DataWrapperWorkmates>> mediatorLiveData = new MediatorLiveData<>();
    private ArrayList<User> users;
    private final ArrayList<DataWrapperWorkmates> datas = new ArrayList<>();

    public WorkmatesRepository() {
        mediatorLiveData.addSource(usersLiveData, value -> {
//            users = value;

            datas.clear();

            for(User user : value) {
                if(user.getChoiceRestaurant() != null && !TextUtils.isEmpty(user.getChoiceRestaurant())) {
                    usersInterestedSize++;
                }

                datas.add(new DataWrapperWorkmates(user, null));
            }

            for(User user : value) {
                getRestaurantLiveData(user.getChoiceRestaurant(), BuildConfig.google_maps_api_key);
            }
        });

        mediatorLiveData.addSource(restaurantMutableLiveData, restaurant -> {

            Log.d(LOG, "[LOG] --> Restaurant : " + restaurant.getName());

            requestCompleted++;

            if(!datas.isEmpty()) {

                for(DataWrapperWorkmates data : datas) {
                    User user = data.getUser();

                    if(user.getChoiceRestaurant() != null && !TextUtils.isEmpty(user.getChoiceRestaurant())) {
                        if(user.getChoiceRestaurant().equalsIgnoreCase(restaurant.getPlaceId())) {
                            data.setRestaurant(restaurant);
                        }
                    }
                }

                if(usersInterestedSize == requestCompleted) {
                    mediatorLiveData.setValue(datas);
                    Log.d(LOG, "[LOG] --> Set mediator data !");
                    Log.d(LOG, "[LOG] ---------------------------------------------------------------------");
                }

            }

//            if(!users.isEmpty()) {
//                for(User user : users) {
//                    if(user.getChoiceRestaurant() != null && !TextUtils.isEmpty(user.getChoiceRestaurant()) && restaurant != null) {
//
//                        if(user.getChoiceRestaurant().equalsIgnoreCase(restaurant.getPlaceId())) {
//                            DataWrapperWorkmates dataWrapperWorkmates = new DataWrapperWorkmates(user, restaurant);
//                            data.add(dataWrapperWorkmates);
//                        }
//
//                    } else {
//                        DataWrapperWorkmates dataWrapperWorkmates = new DataWrapperWorkmates(user, null);
//                        data.add(dataWrapperWorkmates);
//                    }
//                }
//
//                if(usersInterestedSize == requestCompleted) {
//                    mediatorLiveData.setValue(data);
//                    Log.d(LOG, "[LOG] --> Set mediator data !");
//                    Log.d(LOG, "[LOG] ---------------------------------------------------------------------");
//                }
//            }
        });
    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return restaurantMutableLiveData;
    }

    @Override
    public void getRestaurantLiveData(String place_id, String api_key) {

        if(place_id != null && !TextUtils.isEmpty(place_id)) {
            restaurantAPIService.getRestaurant(place_id, RetrofitClientInstance.NAME, api_key).enqueue(new Callback<PlaceResultDetail>() {
                @Override
                public void onResponse(@NonNull Call<PlaceResultDetail> call, @NonNull Response<PlaceResultDetail> response) {
                    if(response.isSuccessful()) {
                        final PlaceResultDetail body = response.body();
                        if(body != null) {
                            Place place = body.getPlace();
                            Restaurant restaurant = new Restaurant();
                            restaurant.setPlaceId(place_id);
                            restaurant.setName(place.getName());
                            restaurantMutableLiveData.setValue(restaurant);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PlaceResultDetail> call, @NonNull Throwable t) {
                    restaurantMutableLiveData.setValue(null);
                    t.printStackTrace();
                }
            });
        }

    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return usersLiveData;
    }

    @Override
    public void getUsers() {
        ArrayList<User> users = new ArrayList<>();

        UserApi.getUsers()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        List<DocumentSnapshot> documents = task.getResult().getDocuments();
                        if(documents.size() != 0) {
                            for(DocumentSnapshot document : documents) {
                                users.add(document.toObject(User.class));
                            }

                            usersLiveData.setValue(users);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    usersLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    public LiveData<ArrayList<DataWrapperWorkmates>> subscribeDataWorkmates() {
        return mediatorLiveData;
    }
}
