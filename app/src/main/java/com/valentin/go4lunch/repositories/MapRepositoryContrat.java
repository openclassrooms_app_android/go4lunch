package com.valentin.go4lunch.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;

import java.util.ArrayList;

public interface MapRepositoryContrat {
    LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData();
    void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key);
    LiveData<ArrayList<User>> subscribeUsersLiveData();
    void getUsers();
}
