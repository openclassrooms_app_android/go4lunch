package com.valentin.go4lunch.repositories;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.apis.RestaurantAPIService;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.models.Place;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.placedetail.PlaceResultDetail;
import com.valentin.go4lunch.models.placeinfo.Photo;
import com.valentin.go4lunch.models.Restaurant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRepository implements MainRepositoryContrat {

    private static final String LOG = MainRepository.class.getSimpleName();

    private final RestaurantAPIService restaurantAPIService = RetrofitClientInstance.getRetrofitInstance().create(RestaurantAPIService.class);
    private final MutableLiveData<Restaurant> restaurantLiveData = new MutableLiveData<>();
    private final MutableLiveData<User> userLiveData = new MutableLiveData<>();

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return restaurantLiveData;
    }

    @Override
    public void getRestaurantLiveData(Context context, String place_id, String fields, String api_key) {

        if(place_id != null && !TextUtils.isEmpty(place_id)) {
            restaurantAPIService.getRestaurant(place_id, fields, api_key).enqueue(new Callback<PlaceResultDetail>() {
                @Override
                public void onResponse(@NonNull Call<PlaceResultDetail> call, @NonNull Response<PlaceResultDetail> response) {
                    if(response.isSuccessful()) {
                        final PlaceResultDetail body = response.body();
                        if(body != null) {
                            Place place = body.getPlace();
                            Log.d(LOG, "[LOG] --> PLACE : " + place);

                            Restaurant restaurant = new Restaurant();
                            restaurant.setPlaceId(place.getPlaceId());
                            restaurant.setName(place.getName());
                            restaurant.setAddress(place.getVicinity());
                            restaurant.setInternationalPhoneNumber(place.getInternationalPhoneNumber());
                            restaurant.setOpenNow(place.getOpeningHours() != null ? place.getOpeningHours().getOpenNow() : false);
                            restaurant.setRating(place.getRating() != null ? place.getRating() : 0);
                            restaurant.setWebsite(place.getWebsite());

                            Photo photo = place.getPhotos() != null ? place.getPhotos().get(0) : null;
                            String photoUrl = null;
                            if (context != null && photo != null) {
                                photoUrl = context.getResources().getString(R.string.url_api_google_place) + "photo?"
                                        + RetrofitClientInstance.PHOTO_REFERENCE_PARAMETER + "="
                                        + photo.getPhotoReference() + "&" + RetrofitClientInstance.MAX_WIDTH_PARAMETER + "=400&"
                                        + RetrofitClientInstance.MAX_HEIGHT_PARAMETER + "=400&"
                                        + RetrofitClientInstance.KEY_PARAMETER + "=" + context.getResources().getString(R.string.google_api_key);
                                restaurant.setPhotoUrl(photoUrl);
                            }

                            restaurantLiveData.setValue(restaurant);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PlaceResultDetail> call, @NonNull Throwable t) {
                    restaurantLiveData.setValue(null);
                    t.printStackTrace();
                }
            });
        }

    }

    @Override
    public LiveData<User> subscribeUserLiveData() {
        return userLiveData;
    }

    @Override
    public void getUser(String uid) {
        Log.d(LOG, "[LOG] --> Récupération de l'utilisateur " + uid + " via FIRESTORE !");
        UserApi.getUser(uid)
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if(document != null && document.exists()) {
                            User user = document.toObject(User.class);
                            userLiveData.setValue(user);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    userLiveData.setValue(null);
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

}
