package com.valentin.go4lunch.repositories;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.apis.RestaurantAPIService;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.apis.UserApi;
import com.valentin.go4lunch.models.placedetail.PlaceResultDetail;
import com.valentin.go4lunch.models.placeinfo.Photo;
import com.valentin.go4lunch.models.Place;
import com.valentin.go4lunch.models.placeinfo.PlaceResult;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.ui.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListRepository implements ListRepositoryContrat {

    private static final String LOG = ListRepository.class.getSimpleName();

    private static final String PLACE_LOCATION = "PLACE_LOCATION";
    private final RestaurantAPIService restaurantAPIService = RetrofitClientInstance.getRetrofitInstance().create(RestaurantAPIService.class);

    final MutableLiveData<ArrayList<Restaurant>> restaurantsMutableLiveData = new MutableLiveData<>();
    final MutableLiveData<ArrayList<User>> usersInterestedLiveData = new MutableLiveData<>();
    final MutableLiveData<Restaurant> restaurantMutableLiveData = new MutableLiveData<>();

    private boolean lastRestaurantIndex = false;
    private ArrayList<Restaurant> restaurantsList = new ArrayList<>();
    final MediatorLiveData<ArrayList<Restaurant >> mediatorLiveData = new MediatorLiveData<>();

    public ListRepository() {
        mediatorLiveData.addSource(restaurantsMutableLiveData, restaurants -> {

            restaurantsList = restaurants;
            for(Restaurant restaurant : restaurantsList) {
                if(restaurantsList.indexOf(restaurant) == restaurantsList.size() - 1) {
                    lastRestaurantIndex = true;
                }

                getUsersInterestedByRestaurant(restaurant.getPlaceId());
            }

        });

        mediatorLiveData.addSource(usersInterestedLiveData, usersInterested -> {

            if(restaurantsList.size() != 0) {
                for (Restaurant restaurant : restaurantsList) {

                    if(usersInterested.size() != 0){
                        for (User userInterested : usersInterested) {
                            if(restaurant.getPlaceId().equalsIgnoreCase(userInterested.getChoiceRestaurant())) {

                                restaurant.setNbInterested(usersInterested.size());
                                if(lastRestaurantIndex) {
                                    setDataList();
                                }
                                return;
                            }
                        }
                    } else {
                        if(lastRestaurantIndex && restaurantsList.indexOf(restaurant) == restaurantsList.size() - 1) {
                            setDataList();
                        }
                    }

                }
            }

        });
    }

    @Override
    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return restaurantsMutableLiveData;
    }

    @Override
    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {

        restaurantAPIService.getNearbyRestaurant(location, radius, type, api_key).enqueue(new Callback<PlaceResult>() {
            @Override
            public void onResponse(@NonNull Call<PlaceResult> call, @NonNull Response<PlaceResult> response) {
                if (response.body() != null) {

                    ArrayList<Restaurant> restaurants = new ArrayList<>();

                    ArrayList<Place> places = response.body().getPlaces();
                    if(places.size() != 0) {

                        for (int i = 0; i < places.size(); i++) {

                            Location placeLocation = new Location(PLACE_LOCATION);
                            placeLocation.setLatitude(places.get(i).getGeometry().getLocation().getLat());
                            placeLocation.setLongitude(places.get(i).getGeometry().getLocation().getLng());

                            Place place = places.get(i);
                            Restaurant restaurant = new Restaurant();
                            restaurant.setPlaceId(place.getPlaceId());
                            restaurant.setName(place.getName());
                            restaurant.setAddress(place.getVicinity());
                            restaurant.setOpenNow(place.getOpeningHours() != null ? place.getOpeningHours().getOpenNow() : false);
                            restaurant.setRating(place.getRating() != null ? place.getRating() : 0);
                            restaurant.setInternationalPhoneNumber(place.getInternationalPhoneNumber());
                            restaurant.setWebsite(place.getWebsite());
                            restaurant.setDistance(calculationByDistance(context, placeLocation));

                            Photo photo = null;
                            photo = places.get(i).getPhotos() != null ? places.get(i).getPhotos().get(0) : null;
                            if (context != null && photo != null) {
                                String photoUrl = context.getResources().getString(R.string.url_api_google_place) + "photo?"
                                        + RetrofitClientInstance.PHOTO_REFERENCE_PARAMETER + "="
                                        + photo.getPhotoReference() + "&" + RetrofitClientInstance.MAX_WIDTH_PARAMETER + "=400&"
                                        + RetrofitClientInstance.MAX_HEIGHT_PARAMETER + "=400&"
                                        + RetrofitClientInstance.KEY_PARAMETER + "=" + context.getResources().getString(R.string.google_api_key);

//                                    photoUrl = "https://www.caffemazzo.fr/wp-content/uploads/2020/11/logo-mazzo.png";

                                restaurant.setPhotoUrl(photoUrl);
                            }

                            restaurants.add(restaurant);
                        }

                    }

                    restaurantsMutableLiveData.setValue(restaurants);

                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceResult> call, @NonNull Throwable t) {
                restaurantsMutableLiveData.setValue(null);
                t.printStackTrace();
            }
        });

    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return restaurantMutableLiveData;
    }

    @Override
    public void getRestaurantLiveData(String place_id, String fields, String api_key) {

        restaurantAPIService.getRestaurant(place_id, fields, api_key).enqueue(new Callback<PlaceResultDetail>() {
            @Override
            public void onResponse(@NonNull Call<PlaceResultDetail> call, @NonNull Response<PlaceResultDetail> response) {
                if(response.body() != null) {
                    Place place = response.body().getPlace();
                    Restaurant restaurant = new Restaurant();
                    restaurant.setPlaceId(place.getPlaceId());
                    restaurant.setName(place.getName());
                    restaurant.setAddress(place.getVicinity());
                    restaurant.setOpenNow(place.getOpeningHours() != null ? place.getOpeningHours().getOpenNow() : false);
                    restaurant.setRating(place.getRating() != null ? place.getRating() : 0);
                    restaurant.setInternationalPhoneNumber(place.getInternationalPhoneNumber());
                    restaurant.setWebsite(place.getWebsite());
                    restaurantMutableLiveData.setValue(restaurant);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceResultDetail> call, @NonNull Throwable t) {
                restaurantMutableLiveData.setValue(null);
                t.printStackTrace();
            }
        });

    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return usersInterestedLiveData;
    }

    @Override
    public void getUsersInterestedByRestaurant(String placeId) {

        ArrayList<User> users = new ArrayList<>();

        UserApi.getUsers()
                .addOnCompleteListener(task -> {
                    if(task.isSuccessful()) {
                        List<DocumentSnapshot> documents = task.getResult().getDocuments();
                        if(documents.size() != 0) {

                            for(DocumentSnapshot document : documents) {
                                User user = document.toObject(User.class);
                                if(user != null && user.getChoiceRestaurant() != null) {
                                    if(user.getChoiceRestaurant().equalsIgnoreCase(placeId)) {
                                        users.add(user);
                                    }
                                }
                            }
                            usersInterestedLiveData.setValue(users);
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    Log.e(LOG, "[LOG] --> " + e);
                });
    }

    public void setDataList() {
        lastRestaurantIndex = false;
        if(restaurantsList != null && restaurantsList.size() > 0) {
            mediatorLiveData.setValue(restaurantsList);
        }
    }

    public LiveData<ArrayList<Restaurant>> getDataList() {
        Log.d(LOG, "[LOG] --> Get Data list !");
        return mediatorLiveData;
    }

    public double calculationByDistance(Context activityContext, Location placeLocation) {
        double distance = 0;
        if(activityContext != null) {
            distance = MainActivity.getLocation(activityContext.getApplicationContext()).distanceTo(placeLocation);
        }
        return distance;
    }
}
