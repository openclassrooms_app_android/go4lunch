package com.valentin.go4lunch.services;

import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.models.User;

public interface LoginServiceContrat {
    LiveData<DocumentSnapshot> subscribeUserLiveData();
    void getUser(String uid);
    void createUser(User user);
}
