package com.valentin.go4lunch.services;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.UserLike;
import com.valentin.go4lunch.repositories.PlaceDetailRepository;

import java.util.ArrayList;

public class PlaceDetailService implements PlaceDetailServiceContrat {

    private static PlaceDetailService INSTANCE = null;
    private final PlaceDetailRepository placeDetailRepository = new PlaceDetailRepository();

    public synchronized static PlaceDetailService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new PlaceDetailService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return placeDetailRepository.subscribeRestaurantLiveData();
    }

    @Override
    public void getRestaurantLiveData(String place_id, String fields, String api_key) {
        placeDetailRepository.getRestaurantLiveData(place_id, fields, api_key);
    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return placeDetailRepository.subscribeUsersInterestedByRestaurantLiveData();
    }

    @Override
    public void getUsersInterestedByRestaurant(String placeId) {
        placeDetailRepository.getUsersInterestedByRestaurant(placeId);
    }

    @Override
    public void createUserLike(UserLike userLike) {
        placeDetailRepository.createUserLike(userLike);
    }

    @Override
    public LiveData<Boolean> isRestaurantLiked(String uid, String restaurant_id) {
        return placeDetailRepository.isRestaurantLiked(uid, restaurant_id);
    }

    @Override
    public void removeLike(UserLike userLike) {
        placeDetailRepository.removeLike(userLike);
    }

    @Override
    public LiveData<User> subscribeUserLiveData() {
        return placeDetailRepository.subscribeUserLiveData();
    }

    @Override
    public void getUser(String uid) {
        placeDetailRepository.getUser(uid);
    }

    @Override
    public void updateUser(User user) {
        placeDetailRepository.updateUser(user);
    }
}
