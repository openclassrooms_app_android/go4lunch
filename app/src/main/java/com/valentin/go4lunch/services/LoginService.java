package com.valentin.go4lunch.services;

import androidx.lifecycle.LiveData;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.repositories.LoginRepository;

public class LoginService implements LoginServiceContrat {

    private final LoginRepository loginRepository = new LoginRepository();
    private static LoginService INSTANCE = null;

    public synchronized static LoginService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new LoginService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<DocumentSnapshot> subscribeUserLiveData() {
        return loginRepository.subscribeUserLiveData();
    }

    @Override
    public void getUser(String uid) {
        loginRepository.getUser(uid);
    }

    @Override
    public void createUser(User user) {
        loginRepository.createUser(user);
    }
}
