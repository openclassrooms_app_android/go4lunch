package com.valentin.go4lunch.services;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.repositories.MainRepository;

public class MainService implements MainServiceContrat {

    private static MainService INSTANCE = null;
    private final MainRepository mainRepository = new MainRepository();

    public synchronized static MainService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MainService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return mainRepository.subscribeRestaurantLiveData();
    }

    @Override
    public void getRestaurantLiveData(Context context, String place_id, String fields, String api_key) {
        mainRepository.getRestaurantLiveData(context, place_id, fields, api_key);
    }

    @Override
    public LiveData<User> subscribeUserLiveData() {
        return mainRepository.subscribeUserLiveData();
    }

    @Override
    public void getUser(String uid) {
        mainRepository.getUser(uid);
    }
}
