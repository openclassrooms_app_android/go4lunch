package com.valentin.go4lunch.services;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;

import java.util.ArrayList;

public interface ListServiceContrat {
    LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData();
    void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key);

    LiveData<Restaurant> subscribeRestaurantLiveData();
    void getRestaurantLiveData(String place_id, String fields, String api_key);

    LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData();
    void getUsersInterestedByRestaurant(String placeId);
}
