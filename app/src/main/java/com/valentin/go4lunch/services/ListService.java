package com.valentin.go4lunch.services;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.repositories.ListRepository;

import java.util.ArrayList;

public class ListService implements ListServiceContrat {

    private static ListService INSTANCE = null;
    private final ListRepository listRepository = new ListRepository();

    public synchronized static ListService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new ListService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return listRepository.subscribeRestaurantsLiveData();
    }

    @Override
    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {
        listRepository.getRestaurantsLiveData(context, location, radius, type, api_key);
    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return listRepository.subscribeRestaurantLiveData();
    }

    @Override
    public void getRestaurantLiveData(String place_id, String fields, String api_key) {
        listRepository.getRestaurantLiveData(place_id, fields, api_key);
    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return listRepository.subscribeUsersInterestedByRestaurantLiveData();
    }

    @Override
    public void getUsersInterestedByRestaurant(String placeId) {
        listRepository.getUsersInterestedByRestaurant(placeId);
    }

    public LiveData<ArrayList<Restaurant>> getDataList() {
        return listRepository.getDataList();
    }
}
