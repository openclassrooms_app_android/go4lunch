package com.valentin.go4lunch.services;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;

public interface MainServiceContrat {
    LiveData<Restaurant> subscribeRestaurantLiveData();
    void getRestaurantLiveData(Context context, String place_id, String fields, String api_key);
    LiveData<User> subscribeUserLiveData();
    void getUser(String uid);
}
