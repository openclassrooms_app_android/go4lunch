package com.valentin.go4lunch.services;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.wrappers.DataWrapperMap;
import com.valentin.go4lunch.repositories.MapRepository;

import java.util.ArrayList;

public class MapService implements MapServiceContrat {

    private static MapService INSTANCE = null;
    private final MapRepository mapRepository = new MapRepository();

    public synchronized static MapService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MapService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return mapRepository.subscribeRestaurantsLiveData();
    }

    @Override
    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {
        mapRepository.getRestaurantsLiveData(context, location, radius, type, api_key);
    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return mapRepository.subscribeUsersLiveData();
    }

    @Override
    public void getUsers() {
        mapRepository.getUsers();
    }


    public LiveData<ArrayList<DataWrapperMap>> subscribeDataMap() {
        return mapRepository.subscribeDataMap();
    }
}
