package com.valentin.go4lunch.services;

import androidx.lifecycle.LiveData;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.wrappers.DataWrapperWorkmates;
import com.valentin.go4lunch.repositories.WorkmatesRepository;

import java.util.ArrayList;

public class WorkmatesService implements WorkmatesServiceContrat {

    private static WorkmatesService INSTANCE = null;
    private final WorkmatesRepository workmatesRepository = new WorkmatesRepository();

    public synchronized static WorkmatesService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new WorkmatesService();
        }

        return INSTANCE;
    }

    @Override
    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return workmatesRepository.subscribeRestaurantLiveData();
    }

    @Override
    public void getRestaurantLiveData(String place_id, String api_key) {
        workmatesRepository.getRestaurantLiveData(place_id, api_key);
    }

    @Override
    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return workmatesRepository.subscribeUsersLiveData();
    }

    @Override
    public void getUsers() {
        workmatesRepository.getUsers();
    }

    public LiveData<ArrayList<DataWrapperWorkmates>> subscribeDataWorkmates() {
        return workmatesRepository.subscribeDataWorkmates();
    }
}
