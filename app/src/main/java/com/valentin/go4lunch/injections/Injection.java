package com.valentin.go4lunch.injections;

import android.content.Context;

import com.valentin.go4lunch.services.ListService;
import com.valentin.go4lunch.services.LoginService;
import com.valentin.go4lunch.services.MainService;
import com.valentin.go4lunch.services.MapService;
import com.valentin.go4lunch.services.PlaceDetailService;
import com.valentin.go4lunch.services.WorkmatesService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Injection {

    public static LoginService provideLoginServiceSource() {
        return LoginService.getInstance();
    }

    public static MainService provideMainServiceSource() {
        return MainService.getInstance();
    }

    public static MapService provideMapServiceSource() {
        return MapService.getInstance();
    }

    public static ListService provideListServiceSource() {
        return ListService.getInstance();
    }

    public static PlaceDetailService providePlaceDetailServiceSource() {
        return PlaceDetailService.getInstance();
    }

    public static WorkmatesService provideWorkmatesServiceSource() {
        return WorkmatesService.getInstance();
    }

    public static Executor provideExecutor(){ return Executors.newSingleThreadExecutor(); }

    public static LoginViewModelFactory provideLoginViewModelFactory() {
        LoginService loginService = provideLoginServiceSource();
        Executor executor = provideExecutor();
        return new LoginViewModelFactory(loginService, executor);
    }

    public static MainViewModelFactory provideMainViewModelFactory() {
        MainService mainService = provideMainServiceSource();
        Executor executor = provideExecutor();
        return new MainViewModelFactory(mainService, executor);
    }

    public static MapViewModelFactory provideMapViewModelFactory() {
        MapService mapService = provideMapServiceSource();
        Executor executor = provideExecutor();
        return new MapViewModelFactory(mapService, executor);
    }

    public static ListViewModelFactory provideListViewModelFactory() {
        ListService listService = provideListServiceSource();
        Executor executor = provideExecutor();
        return new ListViewModelFactory(listService, executor);
    }

    public static PlaceDetailViewModelFactory providePlaceDetailViewModelFactory() {
        PlaceDetailService placeDetailServiceSource = providePlaceDetailServiceSource();
        Executor executor = provideExecutor();
        return new PlaceDetailViewModelFactory(placeDetailServiceSource, executor);
    }

    public static WorkmatesViewModelFactory provideWorkmatesViewModelFactory() {
        WorkmatesService workmatesService = provideWorkmatesServiceSource();
        Executor executor = provideExecutor();
        return new WorkmatesViewModelFactory(workmatesService, executor);
    }
}
