package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.ListService;
import com.valentin.go4lunch.viewmodels.ListViewModel;
import com.valentin.go4lunch.viewmodels.PlaceDetailViewModel;

import java.util.concurrent.Executor;

public class ListViewModelFactory implements ViewModelProvider.Factory {

    private ListService listService;
    private Executor executor;

    public ListViewModelFactory(ListService listService, Executor executor) {
        this.listService = listService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(ListViewModel.class)) {
            return (T) new ListViewModel(listService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

}
