package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.WorkmatesService;
import com.valentin.go4lunch.viewmodels.WorkmatesViewModel;

import java.util.concurrent.Executor;

public class WorkmatesViewModelFactory implements ViewModelProvider.Factory {

    private final WorkmatesService workmatesService;
    private final Executor executor;

    public WorkmatesViewModelFactory(WorkmatesService workmatesService, Executor executor) {
        this.workmatesService = workmatesService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(WorkmatesViewModel.class)) {
            return (T) new WorkmatesViewModel(workmatesService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

}
