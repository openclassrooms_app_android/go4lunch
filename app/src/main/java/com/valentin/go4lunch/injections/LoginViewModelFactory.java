package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.LoginService;
import com.valentin.go4lunch.services.PlaceDetailService;
import com.valentin.go4lunch.viewmodels.LoginViewModel;
import com.valentin.go4lunch.viewmodels.PlaceDetailViewModel;

import java.util.concurrent.Executor;

public class LoginViewModelFactory implements ViewModelProvider.Factory {

    private final LoginService loginService;
    private final Executor executor;

    public LoginViewModelFactory(LoginService loginService, Executor executor) {
        this.loginService = loginService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(loginService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
