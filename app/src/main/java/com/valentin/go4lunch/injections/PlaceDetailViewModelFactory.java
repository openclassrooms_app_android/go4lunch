package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.PlaceDetailService;
import com.valentin.go4lunch.viewmodels.PlaceDetailViewModel;

import java.util.concurrent.Executor;

public class PlaceDetailViewModelFactory implements ViewModelProvider.Factory {

    private final PlaceDetailService placeDetailService;
    private final Executor executor;

    public PlaceDetailViewModelFactory(PlaceDetailService placeDetailService, Executor executor) {
        this.placeDetailService = placeDetailService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(PlaceDetailViewModel.class)) {
            return (T) new PlaceDetailViewModel(placeDetailService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

}
