package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.MainService;
import com.valentin.go4lunch.viewmodels.MainViewModel;
import com.valentin.go4lunch.viewmodels.PlaceDetailViewModel;

import java.util.concurrent.Executor;

public class MainViewModelFactory implements ViewModelProvider.Factory {

    private final MainService mainService;
    private final Executor executor;

    public MainViewModelFactory(MainService mainService, Executor executor) {
        this.mainService = mainService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(mainService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }

}
