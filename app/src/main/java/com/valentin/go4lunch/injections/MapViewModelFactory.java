package com.valentin.go4lunch.injections;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.valentin.go4lunch.services.MapService;
import com.valentin.go4lunch.viewmodels.MainViewModel;
import com.valentin.go4lunch.viewmodels.MapViewModel;

import java.util.concurrent.Executor;

public class MapViewModelFactory implements ViewModelProvider.Factory {

    private MapService mapService;
    private final Executor executor;

    public MapViewModelFactory(MapService mapService, Executor executor) {
        this.mapService = mapService;
        this.executor = executor;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MapViewModel.class)) {
            return (T) new MapViewModel(mapService, executor);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
