package com.valentin.go4lunch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import androidx.core.content.ContextCompat;

public class MarkerHelper {

    private static final String LOG = MarkerHelper.class.getSimpleName();

    public BitmapDescriptor bitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        Bitmap bitmap = null;

        if(vectorDrawable != null) {
            // below line is use to set bounds to our vector drawable.
            vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

            // below line is use to create a bitmap for our
            // drawable which we have added.
            bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }


        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        if(vectorDrawable != null) {
            // below line is use to draw our
            // vector drawable in canvas.
            vectorDrawable.draw(canvas);
        }

        // after generating our bitmap we are returning our bitmap.
        BitmapDescriptor bitmapDescriptor = null;
        if(bitmap != null) {
            bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return bitmapDescriptor;
    }

    public static Bitmap resize(Drawable image) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        return Bitmap.createScaledBitmap(b, 90, 90, false);
    }

}
