package com.valentin.go4lunch.apis;

import com.valentin.go4lunch.models.placedetail.PlaceResultDetail;
import com.valentin.go4lunch.models.placeinfo.PlaceResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestaurantAPIService {
    @GET("nearbysearch/json")
    Call<PlaceResult> getNearbyRestaurant(@Query("location") String location, @Query("radius") int radius, @Query("type") String type, @Query("key") String api_key);

    @GET("details/json")
    Call<PlaceResultDetail> getRestaurant(@Query("place_id") String place_id, @Query("fields") String fields, @Query("key") String api_key);

//    @GET("test/nearbysearch.json")
//    Call<Result> getNearbyRestaurant(@Query("location") String location, @Query("radius") int radius, @Query("type") String type, @Query("key") String api_key);

//    @GET("test/details.json")
//    Call<PlaceResultDetail> getDetailRestaurant(@Query("place_id") String place_id, @Query("fields") String[] fields, @Query("key") String api_key);
}