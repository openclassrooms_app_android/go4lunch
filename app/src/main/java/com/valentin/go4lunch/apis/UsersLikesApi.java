package com.valentin.go4lunch.apis;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.valentin.go4lunch.models.UserLike;

import java.util.List;

public class UsersLikesApi {
    private static final String COLLECTION_NAME = "users_likes";
    private static final String LOG = UsersLikesApi.class.getSimpleName();

    // --- COLLECTION REFERENCE ---
    public static CollectionReference getUsersLikesCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---
    public static void createUserLike(UserLike userLike) {
        UsersLikesApi.getUsersLikesCollection().document().set(userLike);
    }

    // --- GET ---
    public static LiveData<Boolean> isRestaurantLiked(String uid, String restaurant_id){
        final MutableLiveData<Boolean> isLikedMutableLiveData = new MutableLiveData<>();
        UsersLikesApi.getUsersLikesCollection().get().addOnCompleteListener(task -> {
            if(task.isSuccessful()) {

                QuerySnapshot querySnapshot = task.getResult();
                for(DocumentSnapshot document : querySnapshot) {
                    if(document != null && document.exists()) {
                        UserLike userLike = document.toObject(UserLike.class);
                        if(userLike != null) {
                            if(userLike.getUserId().equals(uid) && userLike.getRestaurantId().equals(restaurant_id)) {
                                isLikedMutableLiveData.setValue(true);
                                return;
                            }
                        }
                    }
                }

                if(isLikedMutableLiveData.getValue() == null) {
                    isLikedMutableLiveData.setValue(false);
                }

            }
        })
        .addOnFailureListener(e -> {
            e.printStackTrace();
            Log.e(LOG, "[LOG] --> Cannot get retrieving in '" + COLLECTION_NAME + "' !");
        });

        return isLikedMutableLiveData;
    }

    // --- DELETE ---
    public static void removeLike(@NonNull UserLike userLike) {
        UsersLikesApi.getUsersLikesCollection().get().addOnCompleteListener(task -> {

            if(task.isSuccessful()) {
                QuerySnapshot querySnapshot = task.getResult();
                for(DocumentSnapshot document : querySnapshot) {
                    if(document != null && document.exists()) {
                        UserLike data = document.toObject(UserLike.class);
                        if(data != null) {
                            if(userLike.getUserId().equals(data.getUserId()) && userLike.getRestaurantId().equals(data.getRestaurantId())) {
                                document.getReference().delete();
                            }
                        }
                    }
                }
            }

        });
    }
}
