package com.valentin.go4lunch.apis;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.valentin.go4lunch.models.User;

import java.util.HashMap;
import java.util.Map;

public class UserApi {
    private static final String COLLECTION_NAME = "users";

    // FIELDS
    private static final String AVATAR_FIELD = "avatar";
    private static final String CHOICE_RESTAURANT_FIELD = "choiceRestaurant";
    private static final String DISPLAY_NAME_FIELD = "displayName";
    private static final String EMAIL_FIELD = "email";
    private static final String ID_FIELD = "id";
    private static final String UID_FIELD = "uid";
    private static final String LOG = UserApi.class.getSimpleName();

    // --- COLLECTION REFERENCE ---
    public static CollectionReference getUsersCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // --- CREATE ---
    public static void createUser(User user) {
        UserApi.getUsersCollection().document(user.getUid()).set(user);
    }

    // --- GET ---
    public static Task<DocumentSnapshot> getUser(String uid){
        return UserApi.getUsersCollection().document(uid).get();
    }

    public static Task<QuerySnapshot> getUsers(){
        return UserApi.getUsersCollection().get();
    }

    // --- UPDATE ---
    public static void updateUser(User user) {
        Map<String, Object> mapUser = new HashMap<>();
        mapUser.put(UserApi.AVATAR_FIELD, user.getAvatar());
        mapUser.put(UserApi.CHOICE_RESTAURANT_FIELD, user.getChoiceRestaurant());
        mapUser.put(UserApi.DISPLAY_NAME_FIELD, user.getDisplayName());
        mapUser.put(UserApi.EMAIL_FIELD, user.getEmail());
        mapUser.put(UserApi.ID_FIELD, user.getId());
        mapUser.put(UserApi.UID_FIELD, user.getUid());

        UserApi.getUsersCollection().document(user.getUid()).update(mapUser);
    }
}
