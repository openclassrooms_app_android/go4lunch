package com.valentin.go4lunch.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.wrappers.DataWrapperWorkmates;
import com.valentin.go4lunch.services.WorkmatesService;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class WorkmatesViewModel extends ViewModel {

    private WorkmatesService workmatesService;
    private Executor executor;

    public WorkmatesViewModel(WorkmatesService workmatesService, Executor executor) {
        this.workmatesService = workmatesService;
        this.executor = executor;
    }

    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return workmatesService.subscribeRestaurantLiveData();
    }

    public void getRestaurantLiveData(String place_id, String api_key) {
        executor.execute(() -> {
            workmatesService.getRestaurantLiveData(place_id, api_key);
        });
    }

    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return workmatesService.subscribeUsersLiveData();
    }

    public void getUsers() {
        executor.execute(() -> {
            workmatesService.getUsers();
        });
    }

    public LiveData<ArrayList<DataWrapperWorkmates>> subscribeDataWorkmates() {
        return workmatesService.subscribeDataWorkmates();
    }
}
