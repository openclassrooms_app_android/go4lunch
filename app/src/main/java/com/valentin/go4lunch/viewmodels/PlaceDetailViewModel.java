package com.valentin.go4lunch.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.UserLike;
import com.valentin.go4lunch.services.PlaceDetailService;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class PlaceDetailViewModel extends ViewModel {

    private final PlaceDetailService placeDetailService;
    private final Executor executor;

    public PlaceDetailViewModel(PlaceDetailService placeDetailService, Executor executor) {
        this.placeDetailService = placeDetailService;
        this.executor = executor;
    }

    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return placeDetailService.subscribeRestaurantLiveData();
    }

    public void getRestaurantLiveData(String place_id, String fields, String api_key) {
        executor.execute(() -> {
            placeDetailService.getRestaurantLiveData(place_id, fields, api_key);
        });
    }

    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return placeDetailService.subscribeUsersInterestedByRestaurantLiveData();
    }

    public void getUsersInterestedByRestaurant(String placeId) {
        executor.execute(() -> {
            placeDetailService.getUsersInterestedByRestaurant(placeId);
        });
    }

    public void createUserLike(UserLike userLike) {
        executor.execute(() -> {
            placeDetailService.createUserLike(userLike);
        });
    }

    public LiveData<Boolean> isRestaurantLiked(String uid, String restaurant_id) {
        return placeDetailService.isRestaurantLiked(uid, restaurant_id);
    }

    public void removeLike(UserLike userLike) {
        executor.execute(() -> {
            placeDetailService.removeLike(userLike);
        });
    }

    public void updateUser(User user) {
        executor.execute(() -> {
            placeDetailService.updateUser(user);
        });
    }

    public LiveData<User> subscribeUserLiveData() {
        return placeDetailService.subscribeUserLiveData();
    }

    public void getUser(String uid) {
        executor.execute(() -> {
            placeDetailService.getUser(uid);
        });
    }
}
