package com.valentin.go4lunch.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.DocumentSnapshot;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.services.LoginService;

import java.util.concurrent.Executor;

public class LoginViewModel extends ViewModel {

    private LoginService loginService;
    private Executor executor;

    public LoginViewModel(LoginService loginService, Executor executor) {
        this.loginService = loginService;
        this.executor = executor;
    }

    public LiveData<DocumentSnapshot> subscribeUserLiveData() {
        return loginService.subscribeUserLiveData();
    }

    public void getUser(String uid) {
        executor.execute(() -> {
            loginService.getUser(uid);
        });
    }

    public void createUser(User user) {
        executor.execute(() -> {
            loginService.createUser(user);
        });
    }
}
