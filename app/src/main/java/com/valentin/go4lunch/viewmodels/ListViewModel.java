package com.valentin.go4lunch.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.services.ListService;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class ListViewModel extends ViewModel {

    private ListService listService;
    private Executor executor;

    public ListViewModel(ListService listService, Executor executor) {
        this.listService = listService;
        this.executor = executor;
    }

    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return listService.subscribeRestaurantsLiveData();
    }

    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {
        executor.execute(() -> {
            listService.getRestaurantsLiveData(context, location, radius, type, api_key);
        });
    }

    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return listService.subscribeRestaurantLiveData();
    }
    public void getRestaurantLiveData(String place_id, String fields, String api_key) {
        listService.getRestaurantLiveData(place_id, fields, api_key);
    }

    public LiveData<ArrayList<User>> subscribeUsersInterestedByRestaurantLiveData() {
        return listService.subscribeUsersInterestedByRestaurantLiveData();
    }
    public void getUsersInterestedByRestaurant(String placeId) {
        listService.getUsersInterestedByRestaurant(placeId);
    }

    public LiveData<ArrayList<Restaurant>> getDataList() {
        return listService.getDataList();
    }
}
