package com.valentin.go4lunch.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.wrappers.DataWrapperMap;
import com.valentin.go4lunch.services.MapService;

import java.util.ArrayList;
import java.util.concurrent.Executor;

public class MapViewModel extends ViewModel {

    private MapService mapService;
    private Executor executor;

    public MapViewModel(MapService mapService, Executor executor) {
        this.mapService = mapService;
        this.executor = executor;
    }

    public LiveData<ArrayList<Restaurant>> subscribeRestaurantsLiveData() {
        return mapService.subscribeRestaurantsLiveData();
    }

    public void getRestaurantsLiveData(Context context, String location, int radius, String type, String api_key) {
        executor.execute(() -> {
            mapService.getRestaurantsLiveData(context, location, radius, type, api_key);
        });
    }

    public LiveData<ArrayList<User>> subscribeUsersLiveData() {
        return mapService.subscribeUsersLiveData();
    }

    public void getUsers() {
        executor.execute(() -> {
            mapService.getUsers();
        });
    }

    public LiveData<ArrayList<DataWrapperMap>> subscribeDataMap() {
        return mapService.subscribeDataMap();
    }

}
