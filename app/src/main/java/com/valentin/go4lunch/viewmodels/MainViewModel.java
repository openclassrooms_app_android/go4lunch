package com.valentin.go4lunch.viewmodels;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.services.MainService;

import java.util.concurrent.Executor;

public class MainViewModel extends ViewModel {

    private MainService mainService;
    private Executor executor;

    public MainViewModel(MainService mainService, Executor executor) {
        this.mainService = mainService;
        this.executor = executor;
    }

    public LiveData<Restaurant> subscribeRestaurantLiveData() {
        return mainService.subscribeRestaurantLiveData();
    }

    public void getRestaurantLiveData(Context context, String place_id, String fields, String api_key) {
        executor.execute(() -> {
            mainService.getRestaurantLiveData(context, place_id, fields, api_key);
        });
    }

    public LiveData<User> subscribeUserLiveData() {
        return mainService.subscribeUserLiveData();
    }

    public void getUser(String uid) {
        executor.execute(() -> {
            mainService.getUser(uid);
        });
    }
}
