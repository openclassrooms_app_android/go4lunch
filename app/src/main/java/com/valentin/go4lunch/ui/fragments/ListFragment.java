package com.valentin.go4lunch.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.valentin.go4lunch.R;
import com.valentin.go4lunch.adapters.RestaurantListAdapter;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.ListViewModelFactory;
import com.valentin.go4lunch.listeners.ListFragmentListener;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.ui.login.LoginActivity;
import com.valentin.go4lunch.ui.main.MainActivity;
import com.valentin.go4lunch.ui.place.PlaceDetailActivity;
import com.valentin.go4lunch.utils.PreferencesHelper;
import com.valentin.go4lunch.viewmodels.ListViewModel;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragment extends Fragment implements ListFragmentListener {

    private static final String LOG = ListFragment.class.getSimpleName();
    private static final String RESTAURANTS_LIST = "RESTAURANTS_LIST";

    private ListViewModel listViewModel;
    private ArrayList<Restaurant> restaurants = new ArrayList<>();
    private Restaurant restaurant;
    private RecyclerView mRecyclerView;
    private RestaurantListAdapter adapter;
    private ConstraintLayout mContainerEmptyView;
    private Bundle state;

    public ListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ListFragment.
     */
    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListViewModelFactory listViewModelFactory = Injection.provideListViewModelFactory();
        listViewModel = new ViewModelProvider(this, listViewModelFactory).get(ListViewModel.class);

        listViewModel.subscribeRestaurantLiveData().observe(this, value -> {
            if(restaurant != null) {
                restaurant.setWebsite(value.getWebsite());
                restaurant.setInternationalPhoneNumber(value.getInternationalPhoneNumber());
                if(getActivity() != null) {
                    startActivity(PlaceDetailActivity.newIntent(getContext(), getActivity().getIntent().getStringExtra(LoginActivity.UID_KEY), restaurant));
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState != null) {
            state = savedInstanceState;
        }

        Log.d(LOG, "[LOG] --> onViewCreated !");

        mContainerEmptyView = view.findViewById(R.id.container_empty_view_fragment_list);
        mRecyclerView = view.findViewById(R.id.recycler_view_fragment_list);
        if(getContext() != null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
            dividerItemDecoration.setDrawable(Objects.requireNonNull(ResourcesCompat.getDrawable(getContext().getResources(), R.drawable.divider, getContext().getTheme())));
            mRecyclerView.addItemDecoration(dividerItemDecoration);
        }

        listViewModel.getDataList().observe(getViewLifecycleOwner(), restaurantsList -> {
            restaurants = restaurantsList;
            setView(restaurants);
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        boolean refreshList = false;
        if(getContext() != null) {
            refreshList = PreferencesHelper.getSharedPreferenceBoolean(getContext(), PlaceDetailActivity.REFRESH_LIST, false);
        }

        if(refreshList) {

            if(getContext() != null) {
                PreferencesHelper.setSharedPreferenceBoolean(getContext(), PlaceDetailActivity.REFRESH_LIST, false);

                String location = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null))
                        + "," + Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
                listViewModel.getRestaurantsLiveData(getContext(), location, 500, RetrofitClientInstance.RESTAURANT_TYPE, getContext().getResources().getString(R.string.google_api_key));
            }

        } else {

            if(state != null) {
                restaurants = state.getParcelableArrayList(RESTAURANTS_LIST);
                setView(restaurants);
            } else {

                if(getContext() != null) {
                    String location = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null))
                            + "," + Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
                    listViewModel.getRestaurantsLiveData(getContext(), location, 500, RetrofitClientInstance.RESTAURANT_TYPE, getContext().getResources().getString(R.string.google_api_key));
                }

            }

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(RESTAURANTS_LIST, restaurants);
    }

    public void setView(ArrayList<Restaurant> restaurants) {
        if(restaurants.size() > 0) {
            mContainerEmptyView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);

            if(adapter == null) {
                adapter = new RestaurantListAdapter(this, restaurants);
                mRecyclerView.setAdapter(adapter);
            }
            adapter.updateData(restaurants);
        } else {
            mContainerEmptyView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClickedItem(Restaurant value) {
        restaurant = value;
        if(getContext() != null) {
            String fields = RetrofitClientInstance.NAME + "," + RetrofitClientInstance.INTERNATIONAL_PHONE_NUMBER + "," + RetrofitClientInstance.WEBSITE;
            listViewModel.getRestaurantLiveData(value.getPlaceId(), fields, getContext().getResources().getString(R.string.google_api_key));
        }
    }
}