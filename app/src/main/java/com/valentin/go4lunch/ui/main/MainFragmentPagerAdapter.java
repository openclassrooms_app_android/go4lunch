package com.valentin.go4lunch.ui.main;

import android.util.Log;

import com.valentin.go4lunch.ui.fragments.ListFragment;
import com.valentin.go4lunch.ui.fragments.MapFragment;
import com.valentin.go4lunch.ui.fragments.WorkmatesFragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MainFragmentPagerAdapter extends FragmentStateAdapter {

    private final int nbPage;

    public MainFragmentPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, int nbPage) {
        super(fragmentManager, lifecycle);
        this.nbPage = nbPage;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = MapFragment.getInstance();
        switch(position) {
            case 0:
                fragment = MapFragment.getInstance();
                break;
            case 1:
                fragment = ListFragment.newInstance();
                break;
            case 2:
                fragment = WorkmatesFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return nbPage;
    }
}
