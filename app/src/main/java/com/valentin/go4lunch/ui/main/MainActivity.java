package com.valentin.go4lunch.ui.main;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.MainViewModelFactory;
import com.valentin.go4lunch.receivers.NetworkChangeBroadcastReceiver;
import com.valentin.go4lunch.ui.login.LoginActivity;
import com.valentin.go4lunch.ui.place.PlaceDetailActivity;
import com.valentin.go4lunch.utils.LocationHelper;
import com.valentin.go4lunch.utils.PreferencesHelper;
import com.valentin.go4lunch.utils.ZoomOutPageTransformer;
import com.valentin.go4lunch.viewmodels.MainViewModel;


public class MainActivity extends AppCompatActivity {

    private static final String LOG = MainActivity.class.getSimpleName();

    public static final int REQUEST_CHECK_SETTINGS = 10001;
    public static final String CURRENT_LOCATION = "CURRENT_LOCATION";
    public static final String CURRENT_LOCATION_LATITUDE = "CURRENT_LOCATION_LATITUDE";
    public static final String CURRENT_LOCATION_LONGITUDE = "CURRENT_LOCATION_LONGITUDE";
    public static final String SELECTED_ITEM_ID_BOTTOM_NAVIGATION = "SELECTED_ITEM_ID_BOTTOM_NAVIGATION";
    public static final String REQUEST_PERMISSION_LOCATION = "REQUEST_PERMISSION_LOCATION";
    public static final String REQUEST_ACTIVATION_LOCATION = "REQUEST_ACTIVATION_LOCATION";
    public static final String PERMISSION_DIALOG_IS_SHOWING = "PERMISSION_DIALOG_IS_SHOWING";
    public static final String EXIT_DIALOG_IS_SHOWING = "EXIT_DIALOG_IS_SHOWING";

    private BroadcastReceiver locationBroadcastReceiver;
    private final NetworkChangeBroadcastReceiver networkChangeBroadcastReceiver = new NetworkChangeBroadcastReceiver();
    private Point deviceResolution;
    private LocationManager locationManager;
    private LocationHelper locationHelper;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private ActivityResultLauncher<String> locationPermissionRequest;
    private boolean lunchItemClicked;
    private MainViewModel mainViewModel;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    private BottomNavigationView bottomNavigationView;
    private int bottomNavigationViewItemSelectedId;
    private ViewPager2 viewPager;
    private Dialog permissionLocationDialog;
    private boolean permissionLocationDialogIsShowing;
    private Dialog activationLocationDialog;

    public static Intent newIntent(Context context, String uid) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(LoginActivity.UID_KEY, uid);
        return intent;
    }

    /**
     * Lifecycle Methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        this.configureViewModel();

        // SUBSCRIBE LIVE DATAS
        mainViewModel.subscribeRestaurantLiveData().observe(this, restaurantValue -> {
            if(lunchItemClicked) {
                lunchItemClicked = false;
                startActivity(PlaceDetailActivity.newIntent(this, getIntent().getStringExtra(LoginActivity.UID_KEY), restaurantValue));
            }
        });

        mainViewModel.subscribeUserLiveData().observe(this, user -> {
            if(lunchItemClicked) {
                if(user.getChoiceRestaurant() != null && !TextUtils.isEmpty(user.getChoiceRestaurant())) {
                    String fields = RetrofitClientInstance.PLACE_ID
                            + "," + RetrofitClientInstance.NAME
                            + "," + RetrofitClientInstance.PHOTOS
                            + "," + RetrofitClientInstance.VICINITY
                            + "," + RetrofitClientInstance.INTERNATIONAL_PHONE_NUMBER
                            + "," + RetrofitClientInstance.OPENING_HOURS
                            + "," + RetrofitClientInstance.WEBSITE
                            + "," + RetrofitClientInstance.RATING;
                    mainViewModel.getRestaurantLiveData(this, user.getChoiceRestaurant(), fields, getResources().getString(R.string.google_api_key));
                } else {
                    if(getSupportActionBar() != null) {
                        getSupportActionBar().setTitle(getResources().getString(R.string.bottom_navigation_menu_list_view));
                    }

                    if(viewPager != null) {
                        viewPager.setCurrentItem(1, true);
                    }

                    bottomNavigationView.setSelectedItemId(bottomNavigationView.getMenu().getItem(1).getItemId());

                    drawer.closeDrawer(GravityCompat.START);
                }
            } else {
                View headerNavigationView = navigationView.getHeaderView(0);
                ImageView mAvatar = headerNavigationView.findViewById(R.id.nav_header_main_avatar);
                TextView mName = headerNavigationView.findViewById(R.id.nav_header_main_name);
                TextView mEmail = headerNavigationView.findViewById(R.id.nav_header_main_email);

                // Get picture URL from Firebase
                if (!TextUtils.isEmpty(user.getAvatar()) && user.getAvatar() != null) {
                    Glide.with(MainActivity.this)
                            .load(user.getAvatar())
                            .apply(RequestOptions.circleCropTransform())
                            .into(mAvatar);
                }

                mName.setText(user.getDisplayName());
                mEmail.setText(user.getEmail());
            }
        });

        // GET DEVICE RESOLUTION
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        deviceResolution = new Point();
        wm.getDefaultDisplay().getRealSize(deviceResolution);

        locationHelper = new LocationHelper(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);


        // Create location permission request
        locationPermissionRequest = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if(isGranted) {
                if(permissionLocationDialog != null) {
                    permissionLocationDialog.dismiss();
                }

                // Check if location is activated
                if(locationHelper.isGPSEnabled(locationManager)) {
                    this.configureBottomView();
                    this.configureViewPager();
                    this.setCurrentLocation();
                } else {
                    this.customDialog(REQUEST_ACTIVATION_LOCATION);
                }
            }
        });

        this.setCurrentLocation();

        this.configureToolbar();

        this.configureNavigationDrawer();
    }


    @Override
    protected void onStart() {
        registerReceiver(networkChangeBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        locationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(!permissionLocationDialogIsShowing) {
                    customDialog(REQUEST_ACTIVATION_LOCATION);
                }

                if(activationLocationDialog != null && locationHelper.isGPSEnabled(locationManager)) {
                    activationLocationDialog.dismiss();
                }
            }
        };
        registerReceiver(locationBroadcastReceiver, new IntentFilter(LocationManager.MODE_CHANGED_ACTION));
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Check if location permission is granted
        if(checkLocationPermission(this)) {

            // Check if permission dialog is showing
            if(permissionLocationDialog != null && permissionLocationDialogIsShowing) {
                permissionLocationDialog.dismiss();
                permissionLocationDialogIsShowing = false;
            }

            // Check if location is activated
            if(locationHelper.isGPSEnabled(locationManager)) {
                this.configureBottomView();
                this.configureViewPager();
                this.setCurrentLocation();
            } else {
                this.customDialog(REQUEST_ACTIVATION_LOCATION);
            }
        } else {
            this.customDialog(REQUEST_PERMISSION_LOCATION);
        }

    }

    @Override
    protected void onStop() {
        unregisterReceiver(networkChangeBroadcastReceiver);
        unregisterReceiver(locationBroadcastReceiver);
        super.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        if(checkLocationPermission(this) && locationHelper.isGPSEnabled(locationManager)) {
            savedInstanceState.putInt(SELECTED_ITEM_ID_BOTTOM_NAVIGATION, bottomNavigationView.getSelectedItemId());
        }

        if(!checkLocationPermission(this) && permissionLocationDialog != null) {
            savedInstanceState.putBoolean(PERMISSION_DIALOG_IS_SHOWING, permissionLocationDialog.isShowing());
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(checkLocationPermission(this) && locationHelper.isGPSEnabled(locationManager)) {
            // Save selected item bottom navigation
            bottomNavigationViewItemSelectedId = savedInstanceState.getInt(SELECTED_ITEM_ID_BOTTOM_NAVIGATION);
        }

        if(!checkLocationPermission(this) && permissionLocationDialog != null) {
            permissionLocationDialogIsShowing = savedInstanceState.getBoolean(PERMISSION_DIALOG_IS_SHOWING);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result of activation location request
        if(requestCode == MainActivity.REQUEST_CHECK_SETTINGS) {
            if(resultCode == RESULT_OK) {
                if(activationLocationDialog != null) {
                    activationLocationDialog.dismiss();
                }

                this.setCurrentLocation();
            }
        }

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if(permissionLocationDialog != null && permissionLocationDialog.isShowing()) {
                permissionLocationDialog.dismiss();
            }

            if(activationLocationDialog != null && activationLocationDialog.isShowing()) {
                activationLocationDialog.dismiss();
            }

            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_Go4Lunch_Dialog))
                    .setTitle(R.string.text_exit_dialog)
                    .setCancelable(true)
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        finishAffinity();
                    }).create().show();


        }
    }

    /******************************************************/



    public static boolean checkLocationPermission(Context context) {
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    public static Location getLocation(Context context) {
        Location location = new Location(MainActivity.CURRENT_LOCATION);
        location.setLatitude(Double.parseDouble(PreferencesHelper.getSharedPreferenceString(context, CURRENT_LOCATION_LATITUDE, null)));
        location.setLongitude(Double.parseDouble(PreferencesHelper.getSharedPreferenceString(context, CURRENT_LOCATION_LONGITUDE, null)));
        return location;
    }

    private void setCurrentLocation() {
        if(checkLocationPermission(this)) {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(location -> {
                        if(location != null) {
                            PreferencesHelper.setSharedPreferenceString(this, CURRENT_LOCATION_LATITUDE, String.valueOf(location.getLatitude()));
                            PreferencesHelper.setSharedPreferenceString(this, CURRENT_LOCATION_LONGITUDE, String.valueOf(location.getLongitude()));
                        }
                    })
                    .addOnFailureListener(e -> {
                        Log.e(LOG, "[LOG] --> Error Location : " + e);
                    });
        }
    }

    private void configureViewPager() {
        if(viewPager == null) {
            viewPager = findViewById(R.id.main_activity_view_pager_container);

            viewPager.setAdapter(
                    new MainFragmentPagerAdapter(getSupportFragmentManager(), getLifecycle(), bottomNavigationView.getMenu().size())
            );
            viewPager.setPageTransformer(new ZoomOutPageTransformer());
            viewPager.setUserInputEnabled(false);
        }
    }

    private void configureViewModel(){
        MainViewModelFactory mainViewModelFactory = Injection.provideMainViewModelFactory();
        mainViewModel = new ViewModelProvider(this, mainViewModelFactory).get(MainViewModel.class);
    }

    private void configureToolbar() {
        toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_dehaze);
            getSupportActionBar().setTitle(R.string.bottom_navigation_menu_map_view);
        }
    }



    /* CUSTOM DIALOG */
    private boolean checkIfUserCheckedNeverAskAgain() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION));
        }
        return false;
    }

    private void customDialog(String which) {

        int dialogWidth = (int) (deviceResolution.x * 0.80);
        int dialogHeight = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? ViewGroup.LayoutParams.WRAP_CONTENT : deviceResolution.y;

        if (which.equals(REQUEST_PERMISSION_LOCATION)) {

            if(permissionLocationDialog == null) {
                permissionLocationDialog = new Dialog(MainActivity.this);
            }
            permissionLocationDialog.setContentView(R.layout.custom_location_dialog);
            permissionLocationDialog.setCancelable(false);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                permissionLocationDialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
            }
            permissionLocationDialog.getWindow().setLayout(dialogWidth, dialogHeight);
            permissionLocationDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;

            TextView mTitle = permissionLocationDialog.findViewById(R.id.title_custom_location_dialog);
            TextView mMessage = permissionLocationDialog.findViewById(R.id.message_custom_location_dialog);
            Button mActionButton = permissionLocationDialog.findViewById(R.id.action_button_custom_location_dialog);

            mTitle.setText(R.string.title_location_permission_dialog);
            mMessage.setText(R.string.message_location_permission_dialog);
            mActionButton.setText(R.string.action_button_location_permission_dialog);
            mActionButton.setOnClickListener(v -> {
                if(this.checkIfUserCheckedNeverAskAgain()) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                } else {
                    locationPermissionRequest.launch(Manifest.permission.ACCESS_FINE_LOCATION);
                }
            });

            permissionLocationDialog.show();
            permissionLocationDialogIsShowing = true;
        }


        if(which.equals(REQUEST_ACTIVATION_LOCATION)) {

            if(activationLocationDialog == null) {
                activationLocationDialog = new Dialog(MainActivity.this);
            }
            activationLocationDialog.setContentView(R.layout.custom_location_dialog);
            activationLocationDialog.setCancelable(false);
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                activationLocationDialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.dialog_background));
            }
            activationLocationDialog.getWindow().setLayout((int) (deviceResolution.x * 0.80), ViewGroup.LayoutParams.WRAP_CONTENT);
            activationLocationDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;

            TextView mTitle = activationLocationDialog.findViewById(R.id.title_custom_location_dialog);
            TextView mMessage = activationLocationDialog.findViewById(R.id.message_custom_location_dialog);
            Button mActionButton = activationLocationDialog.findViewById(R.id.action_button_custom_location_dialog);

            mTitle.setText(R.string.title_location_activation_dialog);
            mMessage.setText(R.string.message_location_activation_dialog);
            mActionButton.setText(R.string.action_button_location_activation_dialog);
            mActionButton.setOnClickListener(v -> locationHelper.turnOnGPS());

            activationLocationDialog.show();
        }

    }



    /* NAVIGATION DRAWER */
    private void configureNavigationDrawer() {
        navigationView = findViewById(R.id.main_activity_navigation_drawer);
        drawer = findViewById(R.id.main_activity_wrapper);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mainViewModel.getUser(getIntent().getStringExtra(LoginActivity.UID_KEY));

        navigationView.getMenu().findItem(R.id.activity_main_drawer_lunch)
                .setOnMenuItemClickListener(item -> {
                    lunchItemClicked = true;
                    mainViewModel.getUser(getIntent().getStringExtra(LoginActivity.UID_KEY));
                    Log.d(LOG, "[LOG] --> Clicked menu item !");
                    return true;
                });

        navigationView.getMenu().findItem(R.id.activity_main_drawer_logout)
                .setOnMenuItemClickListener(item -> {

                    Log.d(LOG, "[LOG] --> Attempt logout user !");

                    AuthUI.getInstance().signOut(this)
                            .addOnCompleteListener(this, task -> {
                                if(task.isSuccessful()) {
//                                    finish();
                                    startActivity(LoginActivity.newIntent(this));
                                }
                            });

//                    FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
//                    FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
//
//                    if(mFirebaseUser != null) {
//                        mFirebaseAuth.signOut();
//                    }

                    return true;
                });
    }



    /* BOTTOM NAVIGATION */
    private void configureBottomView() {
        bottomNavigationView = findViewById(R.id.main_activity_bottom_navigation);

        if(bottomNavigationViewItemSelectedId != 0) {
            bottomNavigationView.setSelectedItemId(bottomNavigationViewItemSelectedId);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(this::updateFragment);
    }

    private Boolean updateFragment(MenuItem item){
        // SET TITLE TOOLBAR
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(item.getTitle());
        }

        // UPDATE FRAGMENT VIEW
        if(item.getItemId() == R.id.bottom_navigation_menu_map_view) {
            viewPager.setCurrentItem(0, true);
        } else if(item.getItemId() == R.id.bottom_navigation_menu_list_view) {
            viewPager.setCurrentItem(1, true);
        } else if(item.getItemId() == R.id.bottom_navigation_menu_workmates) {
            viewPager.setCurrentItem(2, true);
        }
        return true;
    }
}
