package com.valentin.go4lunch.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.apis.RetrofitClientInstance;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.MapViewModelFactory;
import com.valentin.go4lunch.models.wrappers.DataWrapperMap;
import com.valentin.go4lunch.ui.main.MainActivity;
import com.valentin.go4lunch.utils.MapStateManager;
import com.valentin.go4lunch.utils.MarkerHelper;
import com.valentin.go4lunch.utils.PreferencesHelper;
import com.valentin.go4lunch.viewmodels.MapViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MapFragment#getInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String LOG = MapFragment.class.getSimpleName();

    private static final float DEFAULT_ZOOM = 17f;
    private static final int ANIMATION_CAMERA_DURATION = 1000;
    private static final String ANIMATION_HAS_BEEN_EXECUTED = "ANIMATION_HAS_BEEN_EXECUTED";

    private MapViewModel mapViewModel;
    private GoogleMap mMap;
    private boolean animationHasBeenExecuted;
    private ImageView locationButton;

    private static MapFragment INSTANCE;

    public MapFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MapFragment.MapFragment
     */
    public static MapFragment getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MapFragment();
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapViewModelFactory mapViewModelFactory = Injection.provideMapViewModelFactory();
        mapViewModel = new ViewModelProvider(this, mapViewModelFactory).get(MapViewModel.class);

        if(savedInstanceState == null) {
            animationHasBeenExecuted = false;
        } else {
            animationHasBeenExecuted = savedInstanceState.getBoolean(ANIMATION_HAS_BEEN_EXECUTED);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        locationButton = view.findViewById(R.id.location_button_fragment_map);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        this.configureMap();

        locationButton.setOnClickListener(view -> {
            double lat = 0;
            double lng = 0;
            if(getContext() != null) {
                lat = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null));
                lng = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
            }
            LatLng latLng = new LatLng(lat, lng);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM), ANIMATION_CAMERA_DURATION, null);
        });

        if(!animationHasBeenExecuted) {
            this.animateCameraOnMyCurrentPositionAndDisplayAllMarkers();
            animationHasBeenExecuted = true;
        } else {
            this.moveCameraOnMyLastKnownPositionAndDisplayAllMarkers();
        }
    }

    /**
     * Lifecycle Fragment
     */
    @Override
    public void onPause() {
        super.onPause();
        if(getContext() != null) {
            // Save map state
            MapStateManager mapStateManager = new MapStateManager(getContext().getApplicationContext());
            mapStateManager.saveMapState(mMap);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.setupMapIfNeeded();

        if(mMap != null && getContext() != null) {
            if(MainActivity.checkLocationPermission(getContext())) {
                mMap.setMyLocationEnabled(true);
//                this.displayMarkers(mMap);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ANIMATION_HAS_BEEN_EXECUTED, animationHasBeenExecuted);
    }

    /**
     * Custom Methods
     */

    private void setupMapIfNeeded() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_container_fragment_map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        }
    }

    private void configureMap() {
        if(getContext() != null) {
            // Set new map style
            mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                    getContext().getApplicationContext(), R.raw.standard_map_json));
        }

        if(MainActivity.checkLocationPermission(getContext())) {
            // Display my current location
            mMap.setMyLocationEnabled(true);
        }

        // Hide location button for zoom in on my current position
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    private void animateCameraOnMyCurrentPositionAndDisplayAllMarkers() {
        mMap.setOnMapLoadedCallback(() -> {
            if(getContext() != null) {
                if(MainActivity.checkLocationPermission(getContext())) {
                    double lat = 0;
                    double lng = 0;
                    if(getContext() != null) {
                        lat = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null));
                        lng = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), DEFAULT_ZOOM), ANIMATION_CAMERA_DURATION, null);
                }
            }

            // Display all restaurants markers
            this.displayMarkers(mMap);
        });
    }

    private void moveCameraOnMyLastKnownPositionAndDisplayAllMarkers() {
        if(getContext() != null) {
            MapStateManager mapStateManager = new MapStateManager(getContext().getApplicationContext());

            // Resume state of map
            CameraPosition position = mapStateManager.getSavedCameraPosition();
            if(position != null) {
                CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
                mMap.moveCamera(update);
                mMap.setMapType(mapStateManager.getSavedMapType());
                if(MainActivity.checkLocationPermission(getContext())) {
                    mMap.setMyLocationEnabled(true);
                }
            }
        }

        // Display all restaurants markers
         this.displayMarkers(mMap);
    }

    public void displayMarkers(GoogleMap map) {
        mapViewModel.subscribeDataMap().observe(getViewLifecycleOwner(), data -> {
            if(getContext() != null) {
                BitmapDescriptor marker;
                for(DataWrapperMap dataWrapperMap : data) {
                    if(dataWrapperMap.isBooked()) {
                        marker = BitmapDescriptorFactory.fromBitmap(MarkerHelper.resize(ResourcesCompat.getDrawable(getResources(), R.drawable.marker_place_booked, getContext().getTheme())));
                    } else {
                        marker = BitmapDescriptorFactory.fromBitmap(MarkerHelper.resize(ResourcesCompat.getDrawable(getResources(), R.drawable.marker_place, getContext().getTheme())));
                    }
                    map.addMarker(new MarkerOptions().position(dataWrapperMap.getRestaurant().getLatLng()).title(dataWrapperMap.getRestaurant().getName()).icon(marker));
                }
            }
        });

        if(getContext() != null) {
            if(getActivity() != null) {
                mapViewModel.getUsers();
            }

            String location = Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LATITUDE, null))
                    + "," + Double.parseDouble(PreferencesHelper.getSharedPreferenceString(getContext(), MainActivity.CURRENT_LOCATION_LONGITUDE, null));
            mapViewModel.getRestaurantsLiveData(getContext(), location, 500, RetrofitClientInstance.RESTAURANT_TYPE, getContext().getResources().getString(R.string.google_api_key));
        }
    }
}