package com.valentin.go4lunch.ui.place;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.adapters.UserListAdapter;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.PlaceDetailViewModelFactory;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.UserLike;
import com.valentin.go4lunch.ui.login.LoginActivity;
import com.valentin.go4lunch.utils.PreferencesHelper;
import com.valentin.go4lunch.viewmodels.PlaceDetailViewModel;

import java.net.URL;
import java.util.ArrayList;

public class PlaceDetailActivity extends AppCompatActivity {

    private final String LOG = PlaceDetailActivity.class.getSimpleName();

    public static final String USERS_INTERESTED_LIST = "USERS_INTERESTED_LIST";
    public static final String RESTAURANT_KEY = "RESTAURANT";
    public static final String REFRESH_MAP = "REFRESH_MAP";
    public static final String REFRESH_LIST = "REFRESH_LIST";
    public static final String REFRESH_WORKMATES = "REFRESH_WORKMATES";

    private PlaceDetailViewModel placeDetailViewModel;
    private ArrayList<User> usersInterestedList;

    private RecyclerView mInterestedWorkmatesRecyclerView;
    private UserListAdapter adapter;
    private ImageView imageEmptyList;
    private TextView textEmptyList;
    private Point deviceResolution;

    private boolean likeButtonClicked;
    private boolean choiceButtonClicked;

    public static Intent newIntent(Context context, String uid, Restaurant restaurant) {
        Intent intent = new Intent(context, PlaceDetailActivity.class);
        intent.putExtra(LoginActivity.UID_KEY, uid);
        intent.putExtra(PlaceDetailActivity.RESTAURANT_KEY, restaurant);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_detail);

        PlaceDetailViewModelFactory placeDetailViewModelFactory = Injection.providePlaceDetailViewModelFactory();
        placeDetailViewModel = new ViewModelProvider(this, placeDetailViewModelFactory).get(PlaceDetailViewModel.class);

        ImageView mImage = findViewById(R.id.image_restaurant_detail);
        TextView mName = findViewById(R.id.name_restaurant_detail);
        RatingBar mRating = findViewById(R.id.rating_restaurant_detail);
        TextView mAddress = findViewById(R.id.address_restaurant_detail);
        FloatingActionButton mChoiceRestaurantButton = findViewById(R.id.choice_button_restaurant_detail);
        Button mCallButton = findViewById(R.id.call_button_restaurant_detail);
        Button mLikeButton = findViewById(R.id.like_button_restaurant_detail);
        Button mWebsiteButton = findViewById(R.id.website_button_restaurant_detail);
        mInterestedWorkmatesRecyclerView = findViewById(R.id.workmates_interested_restaurant_detail);
        mInterestedWorkmatesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        imageEmptyList = findViewById(R.id.image_empty_list_restaurant_detail);
        textEmptyList = findViewById(R.id.text_empty_list_restaurant_detail);

        Restaurant restaurant = getIntent().getExtras().getParcelable(PlaceDetailActivity.RESTAURANT_KEY);

        // GET DEVICE RESOLUTION
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        deviceResolution = new Point();
        wm.getDefaultDisplay().getRealSize(deviceResolution);

        // SUBSCRIBE LIVE DATAS
        placeDetailViewModel.subscribeUsersInterestedByRestaurantLiveData().observe(this, users -> {
            usersInterestedList = users;
            setView(usersInterestedList);
        });

        placeDetailViewModel.subscribeUserLiveData().observe(this, user -> {
            if(user != null) {
                if (choiceButtonClicked) {

                    choiceButtonClicked = false;

                    if(TextUtils.isEmpty(user.getChoiceRestaurant()) || !user.getChoiceRestaurant().equals(restaurant.getPlaceId())) {
                        user.setChoiceRestaurant(restaurant.getPlaceId());
                        mChoiceRestaurantButton.setImageResource(R.drawable.ic_check_circle);
                    } else if(user.getChoiceRestaurant().equals(restaurant.getPlaceId())) {
                        mChoiceRestaurantButton.setImageResource(R.drawable.ic_add);
                        user.setChoiceRestaurant("");
                    }

                    placeDetailViewModel.updateUser(user);
                    placeDetailViewModel.getUsersInterestedByRestaurant(restaurant.getPlaceId());

                    PreferencesHelper.setSharedPreferenceBoolean(this, REFRESH_MAP, true);
                    PreferencesHelper.setSharedPreferenceBoolean(this, REFRESH_LIST, true);
                    PreferencesHelper.setSharedPreferenceBoolean(this, REFRESH_WORKMATES, true);

                } else {
                    if(!TextUtils.isEmpty(user.getChoiceRestaurant()) && user.getChoiceRestaurant().equals(restaurant.getPlaceId())) {
                        mChoiceRestaurantButton.setImageResource(R.drawable.ic_check_circle);
                    }
                }
            }
        });

        // RESTAURANT IMAGE
        if(restaurant.getPhotoUrl() != null) {
            try {
                URL urlImage = new URL(restaurant.getPhotoUrl());
                Drawable drawable = Drawable.createFromStream(urlImage.openStream(), "src");
                mImage.setBackground(drawable);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mImage.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.no_image_available, getBaseContext().getTheme()));
        }

        // NAME
        mName.setMaxWidth(deviceResolution.x / 2);
        mName.setText(restaurant.getName());

        // RATING
        mRating.setRating(restaurant.getRating());

        // ADDRESS
        mAddress.setText(restaurant.getAddress());


        // CALL BUTTON
        Uri call = Uri.parse("tel:" + restaurant.getInternationalPhoneNumber());
        mCallButton.setOnClickListener(v -> {
            startActivity(new Intent(Intent.ACTION_DIAL, call));
        });


        // LIKE BUTTON
        Observer<Boolean> isRestaurantLikedObserver = isLiked -> {
            if(!likeButtonClicked) {
                if(isLiked) {
                    mLikeButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_favorite_filled, 0, 0);
                    mLikeButton.setText(R.string.like_button_restaurant_detail_liked_text);
                }
            } else {
                likeButtonClicked = false;

                if(isLiked) {
                    placeDetailViewModel.removeLike(new UserLike(getIntent().getStringExtra(LoginActivity.UID_KEY), restaurant.getPlaceId()));
                    mLikeButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_favorite, 0, 0);
                    mLikeButton.setText(R.string.like_button_restaurant_detail_text);
                } else {
                    placeDetailViewModel.createUserLike(new UserLike(getIntent().getStringExtra(LoginActivity.UID_KEY), restaurant.getPlaceId()));
                    mLikeButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_favorite_filled, 0, 0);
                    mLikeButton.setText(R.string.like_button_restaurant_detail_liked_text);
                }
            }
        };
        placeDetailViewModel.isRestaurantLiked(getIntent().getStringExtra(LoginActivity.UID_KEY), restaurant.getPlaceId()).observe(this, isRestaurantLikedObserver);

        mLikeButton.setOnClickListener(v -> {
            likeButtonClicked = true;
            placeDetailViewModel.isRestaurantLiked(getIntent().getStringExtra(LoginActivity.UID_KEY), restaurant.getPlaceId()).observe(this, isRestaurantLikedObserver);
        });


        // WEBSITE BUTTON
        if(restaurant.getWebsite() != null) {
            mWebsiteButton.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(restaurant.getWebsite()));
                startActivity(browserIntent);
            });
        } else {
            mWebsiteButton.setVisibility(View.GONE);
        }

        // LIST OF INTERESTED USERS
        if(savedInstanceState == null) {
            placeDetailViewModel.getUsersInterestedByRestaurant(restaurant.getPlaceId());
        } else {
            usersInterestedList = savedInstanceState.getParcelableArrayList(USERS_INTERESTED_LIST);
            setView(usersInterestedList);
        }

        // CHOICE RESTAURANT BUTTON
        placeDetailViewModel.getUser(getIntent().getStringExtra(LoginActivity.UID_KEY));

        mChoiceRestaurantButton.setOnClickListener(v -> {
            choiceButtonClicked = true;
            placeDetailViewModel.getUser(getIntent().getStringExtra(LoginActivity.UID_KEY));
        });

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(USERS_INTERESTED_LIST, usersInterestedList);
    }

    public void setView(ArrayList<User> usersInterestedList) {
        if(usersInterestedList.size() > 0) {
            mInterestedWorkmatesRecyclerView.setVisibility(View.VISIBLE);
            imageEmptyList.setVisibility(View.GONE);
            textEmptyList.setVisibility(View.GONE);

            if(adapter == null) {
                adapter = new UserListAdapter(usersInterestedList);
                mInterestedWorkmatesRecyclerView.setAdapter(adapter);
            }

            adapter.updateData(usersInterestedList);
        } else {
            mInterestedWorkmatesRecyclerView.setVisibility(View.GONE);
            imageEmptyList.setVisibility(View.VISIBLE);
            textEmptyList.setVisibility(View.VISIBLE);
        }
    }
}
