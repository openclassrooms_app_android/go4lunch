package com.valentin.go4lunch.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.valentin.go4lunch.R;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.LoginViewModelFactory;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.ui.main.MainActivity;
import com.valentin.go4lunch.viewmodels.LoginViewModel;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private final String LOG = LoginActivity.class.getSimpleName();

    public static final String UID_KEY = "UID";

    private FirebaseUser firebaseAuth;
    private LoginViewModel loginViewModel;
    private ActivityResultLauncher<Intent> activityResultLauncher;

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance().getCurrentUser();

        this.configureViewModel();

        // SUBSCRIBE LIVE DATAS
        loginViewModel.subscribeUserLiveData().observe(this, documentSnapshot -> {
            if(firebaseAuth != null) {
                if(!documentSnapshot.exists()) {
                    loginViewModel.createUser(new User(
                            firebaseAuth.getUid(),
                            firebaseAuth.getPhotoUrl() != null ? firebaseAuth.getPhotoUrl().toString() : null,
                            firebaseAuth.getDisplayName(),
                            firebaseAuth.getEmail(),
                            ""
                    ));
                }
                startActivity(MainActivity.newIntent(this, firebaseAuth.getUid()));
            }
        });

        // ACTIVITY RESULT LAUNCHER
        activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            IdpResponse response = IdpResponse.fromResultIntent(result.getData());
            if(result.getResultCode() == RESULT_OK) {
                // Successfully signed in
                Log.d(LOG, "[LOG] --> Connexion établie !");
                loginViewModel.getUser(firebaseAuth.getUid());
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                Log.d(LOG, "[LOG] --> Connexion impossible !");
                if(response != null) {
                    Log.e(LOG, "[LOG] --> " + response.getError());
                }
            }
        });

        this.createSignInIntent();
    }

    private void configureViewModel(){
        LoginViewModelFactory loginViewModelFactory = Injection.provideLoginViewModelFactory();
        this.loginViewModel = new ViewModelProvider(this, loginViewModelFactory).get(LoginViewModel.class);
    }

    public void createSignInIntent() {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.FacebookBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()
        );

        AuthMethodPickerLayout customLayout = new AuthMethodPickerLayout
                .Builder(R.layout.custom_login)
                .setFacebookButtonId(R.id.custom_login_facebook_button)
                .setGoogleButtonId(R.id.custom_login_google_button)
                .build();

        activityResultLauncher.launch(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setIsSmartLockEnabled(false, false)
                .setAuthMethodPickerLayout(customLayout)
                .build());

    }
}
