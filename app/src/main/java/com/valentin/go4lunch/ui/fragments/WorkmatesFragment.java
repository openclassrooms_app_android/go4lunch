package com.valentin.go4lunch.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.valentin.go4lunch.R;
import com.valentin.go4lunch.adapters.WorkmatesListAdapter;
import com.valentin.go4lunch.injections.Injection;
import com.valentin.go4lunch.injections.WorkmatesViewModelFactory;
import com.valentin.go4lunch.models.wrappers.DataWrapperWorkmates;
import com.valentin.go4lunch.ui.place.PlaceDetailActivity;
import com.valentin.go4lunch.utils.PreferencesHelper;
import com.valentin.go4lunch.viewmodels.WorkmatesViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkmatesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkmatesFragment extends Fragment {

    private static final String LOG = WorkmatesFragment.class.getSimpleName();
    public static String DATAS_LIST = "DATAS_LIST";

    private Bundle state;
    private WorkmatesViewModel workmatesViewModel;
    private ArrayList<DataWrapperWorkmates> datas = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private WorkmatesListAdapter adapter;
    private ConstraintLayout mContainerEmptyView;

    public WorkmatesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment WorkmatesFragment.
     */
    public static WorkmatesFragment newInstance() {
        return new WorkmatesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WorkmatesViewModelFactory workmatesViewModelFactory = Injection.provideWorkmatesViewModelFactory();
        workmatesViewModel = new ViewModelProvider(this, workmatesViewModelFactory).get(WorkmatesViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workmates, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState != null) {
            state = savedInstanceState;
        }

        mContainerEmptyView = view.findViewById(R.id.container_empty_view_fragment_workmates);
        mRecyclerView = view.findViewById(R.id.recycler_view_fragment_workmates);
        if(getContext() != null) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        // SUBSCRIBE LIVE DATAS
        workmatesViewModel.subscribeDataWorkmates().observe(getViewLifecycleOwner(), value -> {
            Log.d(LOG, "[LOG] --> Output Mediator : " + value.size());
            Log.d(LOG, "[LOG] ---------------------------------------------------------------------");
            datas = value;
            setView(datas);
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        boolean refreshWorkmates = false;
        if(getContext() != null) {
            refreshWorkmates = PreferencesHelper.getSharedPreferenceBoolean(getContext(), PlaceDetailActivity.REFRESH_WORKMATES, false);
        }

        if(refreshWorkmates) {
            PreferencesHelper.setSharedPreferenceBoolean(getContext(), PlaceDetailActivity.REFRESH_WORKMATES, false);
            workmatesViewModel.getUsers();
        } else {
            if(state == null) {
                workmatesViewModel.getUsers();
            } else {
                datas = state.getParcelableArrayList(DATAS_LIST);
                setView(datas);
            }
        }


    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(DATAS_LIST, datas);
    }

    public void setView(ArrayList<DataWrapperWorkmates> datas) {
        if(datas.size() > 0) {
            mContainerEmptyView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            if(adapter == null) {
                adapter = new WorkmatesListAdapter(datas);
                mRecyclerView.setAdapter(adapter);
            }

            adapter.updateData(datas);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mContainerEmptyView.setVisibility(View.VISIBLE);
        }
    }
}