package com.valentin.go4lunch.listeners;

import com.valentin.go4lunch.models.Restaurant;

public interface ListFragmentListener {
    void onClickedItem(Restaurant value);
}