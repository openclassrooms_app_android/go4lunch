package com.valentin.go4lunch.adapters;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.go4lunch.R;
import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;
import com.valentin.go4lunch.models.wrappers.DataWrapperWorkmates;

import java.net.URL;
import java.util.ArrayList;

public class WorkmatesListAdapter extends RecyclerView.Adapter<WorkmatesListAdapter.WorkmatesViewHolder> {

    private static final String LOG = WorkmatesListAdapter.class.getSimpleName() ;
    private final ArrayList<DataWrapperWorkmates> datas = new ArrayList<>();

    public WorkmatesListAdapter(ArrayList<DataWrapperWorkmates> datas) {
        this.datas.addAll(datas);
    }

    public void updateData(ArrayList<DataWrapperWorkmates> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    public static class WorkmatesViewHolder extends RecyclerView.ViewHolder {

        private final ImageView mImage;
        private final TextView mText;

        public WorkmatesViewHolder(@NonNull View itemView) {
            super(itemView);

            mImage = itemView.findViewById(R.id.avatar_workmates_list_item);
            mText = itemView.findViewById(R.id.text_workmates_list_item);
        }

        public ImageView getImage() {
            return mImage;
        }

        public TextView getText() {
            return mText;
        }
    }

    @NonNull
    @Override
    public WorkmatesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workmates_list_item, parent, false);
        return new WorkmatesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkmatesViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        User user = datas.get(position).getUser();
        Restaurant restaurant = datas.get(position).getRestaurant();

        if(user.getAvatar() != null) {
            try {
                URL urlImage = new URL(user.getAvatar());
                Drawable drawable = Drawable.createFromStream(urlImage.openStream(), "src");
                holder.getImage().setBackground(drawable);
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(LOG, "[LOG] --> Unable to create image from item N°" + position);
            }
        } else {
            holder.getImage().setBackground(ResourcesCompat.getDrawable(res, R.drawable.no_image_available, holder.itemView.getContext().getTheme()));
        }
        if(user.getChoiceRestaurant() != null && !TextUtils.isEmpty(user.getChoiceRestaurant()) && restaurant != null) {
            holder.getText().setText(res.getString(R.string.user_eating_string, user.getDisplayName(), restaurant.getName()));
        } else {
            holder.getText().setTextColor(Color.GRAY);
            holder.getText().setText(res.getString(R.string.user_not_decided_string, user.getDisplayName()));
        }
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
