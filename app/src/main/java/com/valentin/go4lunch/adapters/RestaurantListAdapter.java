package com.valentin.go4lunch.adapters;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.valentin.go4lunch.R;
import com.valentin.go4lunch.listeners.ListFragmentListener;
import com.valentin.go4lunch.models.Restaurant;

import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

public class RestaurantListAdapter extends RecyclerView.Adapter<RestaurantListAdapter.RestaurantViewHolder> {

    private static final String LOG = RestaurantListAdapter.class.getSimpleName();
    private final ListFragmentListener listFragmentListener;
    private final ArrayList<Restaurant> restaurants = new ArrayList<>();


    public RestaurantListAdapter(ListFragmentListener listFragmentListener, ArrayList<Restaurant> restaurants) {
        this.listFragmentListener = listFragmentListener;
        this.restaurants.addAll(restaurants);
    }

    public void updateData(ArrayList<Restaurant> restaurants) {
        this.restaurants.clear();
        this.restaurants.addAll(restaurants);
        notifyDataSetChanged();
    }

    // RESTAURANT VIEW HOLDER
    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        // RESTAURANT ITEM
        private final ConstraintLayout mWrapperRestaurantListItem;
        private final TextView mTitle;
        private final TextView mAddress;
        private final TextView mSchedule;
        private final TextView mDistance;
        private final TextView mNbInterested;
        private final RatingBar mRating;
        private final ImageView mImage;


        public RestaurantViewHolder(View view) {
            super(view);

            // RESTAURANT LIST
            mWrapperRestaurantListItem = view.findViewById(R.id.wrapper_restaurant_list_item);
            mTitle = view.findViewById(R.id.title_restaurant_list_item);
            mAddress = view.findViewById(R.id.address_restaurant_list_item);
            mSchedule = view.findViewById(R.id.schedule_restaurant_list_item);
            mDistance = view.findViewById(R.id.distance_restaurant_list_item);
            mNbInterested = view.findViewById(R.id.nb_of_interested_restaurant_list_item);
            mRating = view.findViewById(R.id.rating_restaurant_list_item);
            mImage = view.findViewById(R.id.image_restaurant_list_item);
        }

        public ConstraintLayout getWrapperRestaurantListItem() {
            return mWrapperRestaurantListItem;
        }

        public TextView getTitle() {
            return mTitle;
        }

        public TextView getAddress() {
            return mAddress;
        }

        public TextView getSchedule() {
            return mSchedule;
        }

        public TextView getDistance() {
            return mDistance;
        }

        public TextView getNbInterested() {
            return mNbInterested;
        }

        public RatingBar getRating() {
            return mRating;
        }

        public ImageView getImage() {
            return mImage;
        }
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list_item, parent, false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        Restaurant restaurant = restaurants.get(position);
        holder.getWrapperRestaurantListItem().setOnClickListener(v -> listFragmentListener.onClickedItem(restaurant));

        holder.getTitle().setText(restaurant.getName());
        holder.getAddress().setText(restaurant.getAddress());
        holder.getSchedule().setText(restaurant.isOpenNow() ? holder.itemView.getResources().getString(R.string.restaurant_opened) : holder.itemView.getResources().getString(R.string.restaurant_closed));
        holder.getDistance().setText(displayDistance(restaurant.getDistance()));
        String nbInterested = "(" + restaurant.getNbInterested() + ")";
        holder.getNbInterested().setText(nbInterested);
        holder.getRating().setRating(restaurant.getRating());

        if(restaurant.getPhotoUrl() != null) {
            try {
                URL urlImage = new URL(restaurant.getPhotoUrl());
                Drawable drawable = Drawable.createFromStream(urlImage.openStream(), "src");
                holder.getImage().setBackground(drawable);
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(LOG, "[LOG] --> Unable to create image from item N°" + position);
            }
        } else {
            Resources res = holder.itemView.getContext().getResources();
            holder.getImage().setBackground(ResourcesCompat.getDrawable(res, R.drawable.no_image_available, holder.itemView.getContext().getTheme()));
        }
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }


    /* CUSTOM METHODS */

    public String displayDistance(double distance) {
        Formatter formatter = new Formatter();
        if(distance < 1000) {
            return formatter.format("%.0f", distance) + "m";
        }

        return formatter.format("%.1f",(distance / 1000)) + "km";
    }
}
