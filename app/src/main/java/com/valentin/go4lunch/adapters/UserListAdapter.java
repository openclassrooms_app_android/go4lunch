package com.valentin.go4lunch.adapters;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.go4lunch.R;
import com.valentin.go4lunch.models.User;

import java.net.URL;
import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.WorkmatesInterestedViewHolder> {

    private static final String LOG = UserListAdapter.class.getSimpleName();

    private final ArrayList<User> users = new ArrayList<>();

    public UserListAdapter(ArrayList<User> users) {
        this.users.addAll(users);
    }

    public void updateData(ArrayList<User> usersInterestedList) {
        this.users.clear();
        this.users.addAll(usersInterestedList);
        notifyDataSetChanged();
    }

    public static class WorkmatesInterestedViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mImage;
        private final TextView mText;

        public WorkmatesInterestedViewHolder(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.avatar_workmates_interested_list_item);
            mText = itemView.findViewById(R.id.text_workmates_interested_list_item);
        }

        public ImageView getImage() {
            return mImage;
        }

        public TextView getText() {
            return mText;
        }
    }

    @NonNull
    @Override
    public WorkmatesInterestedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workmates_interested_list_item, parent, false);

        return new WorkmatesInterestedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WorkmatesInterestedViewHolder holder, int position) {

        User user = users.get(position);

        if(user.getAvatar() != null) {
            try {
                URL urlImage = new URL(user.getAvatar());
                Drawable drawable = Drawable.createFromStream(urlImage.openStream(), "src");
                holder.getImage().setBackground(drawable);
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(LOG, "[LOG] --> Unable to create image from item N°" + position);
            }
        } else {
            Resources res = holder.itemView.getContext().getResources();
            holder.getImage().setBackground(ResourcesCompat.getDrawable(res, R.drawable.no_image_available, holder.itemView.getContext().getTheme()));
        }

        String text = user.getDisplayName() + " is joining !";
        holder.getText().setText(text);

    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
