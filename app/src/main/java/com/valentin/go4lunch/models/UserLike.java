package com.valentin.go4lunch.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class UserLike implements Parcelable {

    private String userId;
    private String restaurantId;

    public UserLike(String userId, String restaurantId) {
        this.userId = userId;
        this.restaurantId = restaurantId;
    }

    public UserLike() {
    }

    protected UserLike(Parcel in) {
        userId = in.readString();
        restaurantId = in.readString();
    }

    public static final Creator<UserLike> CREATOR = new Creator<UserLike>() {
        @Override
        public UserLike createFromParcel(Parcel in) {
            return new UserLike(in);
        }

        @Override
        public UserLike[] newArray(int size) {
            return new UserLike[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(restaurantId);
    }

    @NonNull
    @Override
    public String toString() {
        return "UserLike{" +
                "userId='" + userId + '\'' +
                ", restaurantId='" + restaurantId + '\'' +
                '}';
    }
}
