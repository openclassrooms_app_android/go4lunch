package com.valentin.go4lunch.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class User implements Parcelable {

    private long id;

    private String uid;

    private String avatar;

    private String displayName;

    private String email;

    private String choiceRestaurant;

    /**
     * Constructor
     * @param uid
     * @param avatar
     * @param displayName
     * @param email
     * @param choiceRestaurant
     */

    public User(String uid, String avatar, String displayName, String email, @Nullable String choiceRestaurant) {
        this.uid = uid;
        this.avatar = avatar;
        this.displayName = displayName;
        this.email = email;
        this.choiceRestaurant = choiceRestaurant;
    }

    public User() {

    }

    protected User(Parcel in) {
        id = in.readLong();
        uid = in.readString();
        avatar = in.readString();
        displayName = in.readString();
        email = in.readString();
        choiceRestaurant = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(uid);
        dest.writeString(avatar);
        dest.writeString(displayName);
        dest.writeString(email);
        dest.writeString(choiceRestaurant);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getChoiceRestaurant() {
        return choiceRestaurant;
    }

    public void setChoiceRestaurant(String choiceRestaurant) {
        this.choiceRestaurant = choiceRestaurant;
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", avatar='" + avatar + '\'' +
                ", displayName='" + displayName + '\'' +
                ", email='" + email + '\'' +
                ", choiceRestaurant='" + choiceRestaurant + '\'' +
                '}';
    }
}
