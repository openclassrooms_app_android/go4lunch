package com.valentin.go4lunch.models.placeinfo;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valentin.go4lunch.models.Place;

import java.util.ArrayList;

public class PlaceResult {

    @SerializedName("results")
    @Expose
    private ArrayList<Place> places = null;

    public ArrayList<Place> getPlaces() {
        return places;
    }

    public void setRestaurant(ArrayList<Place> places) {
        this.places = places;
    }

    @NonNull
    @Override
    public String toString() {
        return "Places{" +
                "results=" + places +
                '}';
    }
}
