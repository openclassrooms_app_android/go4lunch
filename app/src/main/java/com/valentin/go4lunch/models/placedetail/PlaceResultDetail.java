package com.valentin.go4lunch.models.placedetail;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.valentin.go4lunch.models.Place;

public class PlaceResultDetail {
    @SerializedName("result")
    @Expose
    private Place place;

    public PlaceResultDetail() {
    }

    public PlaceResultDetail(Place place) {
        this.place = place;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    @NonNull
    @Override
    public String toString() {
        return "PlaceResultDetail{" +
                "place=" + place +
                '}';
    }
}
