package com.valentin.go4lunch.models.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.valentin.go4lunch.models.User;
import com.valentin.go4lunch.models.Restaurant;

public class DataWrapperWorkmates implements Parcelable {

    private User user;
    private Restaurant restaurant;

    public DataWrapperWorkmates(User user, Restaurant restaurant) {
        this.user = user;
        this.restaurant = restaurant;
    }

    protected DataWrapperWorkmates(Parcel in) {
        user = in.readParcelable(User.class.getClassLoader());
        restaurant = in.readParcelable(Restaurant.class.getClassLoader());
    }

    public static final Creator<DataWrapperWorkmates> CREATOR = new Creator<DataWrapperWorkmates>() {
        @Override
        public DataWrapperWorkmates createFromParcel(Parcel in) {
            return new DataWrapperWorkmates(in);
        }

        @Override
        public DataWrapperWorkmates[] newArray(int size) {
            return new DataWrapperWorkmates[size];
        }
    };

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeParcelable(restaurant, flags);
    }
}
