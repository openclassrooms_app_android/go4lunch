package com.valentin.go4lunch.models.wrappers;

import com.valentin.go4lunch.models.Restaurant;

public class DataWrapperMap {

    private Restaurant restaurant;
    private boolean isBooked;

    public DataWrapperMap(Restaurant restaurant, boolean isBooked) {
        this.restaurant = restaurant;
        this.isBooked = isBooked;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }
}
