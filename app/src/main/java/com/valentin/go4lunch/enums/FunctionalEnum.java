package com.valentin.go4lunch.enums;

public enum FunctionalEnum {
    EMPTY_PLACE_ID, NO_INTERNET, GENERIC_ERROR
}
